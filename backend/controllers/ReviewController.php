<?php

namespace backend\controllers;

use Yii;
use common\models\Review;
use common\models\ReviewSearch;
use himiklab\sortablegrid\SortableGridAction;
use sfog\image\Image;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Review::className(),
            ],
        ];
    }

    /**
     * Lists all Review models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Review model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Review model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Review();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, "image");
            $model->man_image = UploadedFile::getInstance($model, "man_image");

            if ($model->validate()) {
                $sfogImage = new Image(false, 85);
                $model->image = $sfogImage->uploadFile(
                    $model,
                    'image',
                    'image',
                    ['250x350']
                );
                $model->man_image = $sfogImage->uploadFile(
                    $model,
                    'man_image',
                    'man_image',
                    ['40x40']
                );
    
                if ($model->save()) 
                    return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Review model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = $model->image;
        $man_image = $model->man_image;
        

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if (!empty($_FILES['Review']['name']['image'])) {
                    $sfogImage = new Image(false, 85);
                    $model->image = $sfogImage->uploadFile(
                        $model,
                        'image',
                        'image',
                        ['250x350']
                    );
                } else {
                    $model->image = $image;
                }
    
                if (!empty($_FILES['Review']['name']['man_image'])) {
                    $sfogImage = new Image(false, 85);
                    $model->man_image = $sfogImage->uploadFile(
                        $model,
                        'man_image',
                        'man_image',
                        ['40x40']
                    );
                } else {
                    $model->man_image = $man_image;
                }
    
                if ($model->save())
                    return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Review model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
