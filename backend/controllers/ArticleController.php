<?php

namespace backend\controllers;

use Yii;
use common\models\Article;
use common\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use sfog\image\Image as SfogImage;
use backend\models\Model;
use common\models\Content;
use Throwable;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\web\Response;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article;
        $modelsContent = [new Content()];
        $sfogImage = new SfogImage(false, 85);

        if ($model->load(Yii::$app->request->post())) {
            $modelsContent = Model::createMultiple(Content::classname());
            Model::loadMultiple($modelsContent, Yii::$app->request->post());

            foreach($modelsContent as $index => $modelContent) {
                $modelContent->image = UploadedFile::getInstance($modelContent, "[{$index}]image");
                $modelContent->content3_human_image = UploadedFile::getInstance($modelContent, "[{$index}]content3_human_image");
            }

            $model->image = UploadedFile::getInstance($model, "image");
            $model->og_image = UploadedFile::getInstance($model, "og_image");

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsContent),
                    ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsContent) && $valid;
            if ($valid) {
                foreach($modelsContent as $index => $modelContent) {
                    $modelContent->image = $sfogImage->uploadFileDynamicForm(
                        $modelContent,
                        $index,
                        "[{$index}]image",
                        "[{$index}]image",
                        []
                    );

                    $modelContent->content3_human_image = $sfogImage->uploadFileDynamicForm(
                        $modelContent,
                        $index,
                        "[{$index}]content3_human_image",
                        "[{$index}]content3_human_image",
                        ['40x40']
                    );
                }

                if (!empty($_FILES['Article']['name']['image'])) {
                    $model->image = $sfogImage->uploadFile(
                        $model,
                        'image',
                        'image',
                        []
                    );
                }

                if (!empty($_FILES['Article']['name']['og_image'])) {
                    $model->og_image = $sfogImage->uploadFile(
                        $model,
                        'og_image',
                        'og_image',
                        []
                    );
                }

                $transaction = \Yii::$app->db->beginTransaction();

                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsContent as $modelContent) {
                            $modelContent->article_id = $model->id;

                            if (! ($flag = $modelContent->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Throwable $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelsContent' => (empty($modelsContent)) ? [new Content] : $modelsContent,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsContent = $model->contents;
        $sfogImage = new SfogImage(false);
        $image = $model->image;
        $og_image = $model->og_image;

        foreach($modelsContent as $index => $modelContent) {
            $images[$modelContent->id] = $modelContent->image;
            $content3_human_images[$modelContent->id] = $modelContent->content3_human_image;
        }

        if ($model->load(Yii::$app->request->post())) {
            $oldContentIDs = ArrayHelper::map($modelsContent, 'id', 'id');
            $modelsContent = Model::createMultiple(Content::classname(), $modelsContent);
            Model::loadMultiple($modelsContent, Yii::$app->request->post());
            $deletedContentsIDs = array_diff($oldContentIDs, array_filter(ArrayHelper::map($modelsContent, 'id', 'id')));

            foreach($modelsContent as $index => $modelContent) {
                if (!empty($_FILES['Content']['name'][$index]['image'])) {
                    $modelContent->image = UploadedFile::getInstance($modelContent, "[{$index}]image");
                }
                if (!empty($_FILES['Content']['name'][$index]['content3_human_image'])) {
                    $modelContent->content3_human_image = UploadedFile::getInstance($modelContent, "[{$index}]content3_human_image");
                }
            }

            $model->image = UploadedFile::getInstance($model, "image");
            $model->og_image = UploadedFile::getInstance($model, "og_image");

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsContent),
                    ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsContent) && $valid;
            if ($valid) {
                foreach($modelsContent as $index => $modelContent) {
                    if (!empty($_FILES['Content']['name'][$index]['image'])) {
                        $modelContent->image = $sfogImage->uploadFileDynamicForm(
                            $modelContent,
                            $index,
                            "[{$index}]image",
                            "[{$index}]image",
                            []
                        );
                    } else {
                        if ($images[$modelContent->id]) {
                            $modelContent->image = $images[$modelContent->id];
                        }
                    }

                    if (!empty($_FILES['Content']['name'][$index]['content3_human_image'])) {
                        $modelContent->content3_human_image = $sfogImage->uploadFileDynamicForm(
                            $modelContent,
                            $index,
                            "[{$index}]content3_human_image",
                            "[{$index}]content3_human_image",
                            ['40x40']
                        );
                    } else {
                        if ($content3_human_images[$modelContent->id]) {
                            $modelContent->content3_human_image = $content3_human_images[$modelContent->id];
                        }
                    }
                }

                if (!empty($_FILES['Article']['name']['image'])) {
                    $model->image = $sfogImage->uploadFile(
                        $model,
                        'image',
                        'image',
                        []
                    );
                } else {
                    $model->image = $image;
                }

                if (!empty($_FILES['Article']['name']['og_image'])) {
                    $model->og_image = $sfogImage->uploadFile(
                        $model,
                        'og_image',
                        'og_image',
                        []
                    );
                } else {
                    $model->og_image = $og_image;
                }
                

                $transaction = \Yii::$app->db->beginTransaction();

                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedContentsIDs)) {
                            foreach(Content::findAll($deletedContentsIDs) as $modelContent) {
                                $modelContent->delete();
                            }
                        }

                        foreach ($modelsContent as $modelContent) {
                            $modelContent->article_id = $model->id;

                            if (! ($flag = $modelContent->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Throwable $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsContent' => (empty($modelsContent)) ? [new Content] : $modelsContent,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
