<?php

use common\models\Category;
use common\models\Human;
use kartik\widgets\FileInput;
use mihaildev\ckeditor\CKEditor;
use kidzen\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin([
        'id' => 'Article'
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'human_id')->dropDownList(
        ArrayHelper::map(Human::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name')
    ) ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map(Category::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name')
    ) ?>

    <?= $form->field($model, 'icon')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?=$this->render('@backend/views/_image', [
        'form' => $form,
        'model' => $model,
        'image' => $model->image,
        'name' => 'image'
    ])?>

    <?=$this->render('@backend/views/_image', [
        'form' => $form,
        'model' => $model,
        'image' => $model->og_image,
        'name' => 'og_image'
    ])?>

    <?= $form->field($model, 'feature1_red')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'feature1_text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'feature2_red')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'feature2_text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'feature3_red')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'feature3_text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_h1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>

    <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper_content', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-content', // required: css class selector
            'widgetItem' => '.content-item', // required: css class
            'min' => 1, // 0 or 1 (default 1)
            'insertButton' => '.add-content', // css class
            'deleteButton' => '.remove-content', // css class
            'model' => $modelsContent[0],
            'formId' => 'Article',
            'formFields' => [
                'p',
            ],
        ]); ?>

        <div class="panel panel-default">
            <div class="panel-heading" style="height: 45px;">
                <h4 style="float: left;margin: 0;"><i class="glyphicon glyphicon-envelope"></i> Контент</h4>
                <div class="pull-right">
                    <button type="button" class="add-content btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="container-content"><!-- widgetContainer -->
                    <?php foreach ($modelsContent as $i => $modelContent): ?>
                        <div class="content-item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Элемент</h3>
                                <div class="pull-right">
                                    <button type="button" class="remove-content btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                    if (! $modelContent->isNewRecord) {
                                        echo Html::activeHiddenInput($modelContent, "[{$i}]id");
                                    }
                                ?>

                                <h4>Сортировка</h4>
                                <?= $form->field($modelContent, "[{$i}]sort_order")->textInput() ?>
                                <h4>Просто html</h4>
                                <?= $form->field($modelContent, "[{$i}]p")->widget(CKEditor::class) ?>

                                <h4>Изображение</h4>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <?php if (!empty($modelContent->image)) { ?>
                                            <?=Html::img($modelContent->image, ['width' => 250])?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-9">
                                        <?=$form->field($modelContent, "[{$i}]image")->widget(FileInput::className(), [
                                            'pluginOptions' => [
                                                'showCaption' => false,
                                                'showRemove' => false,
                                                'showUpload' => false,
                                                'browseClass' => 'btn btn-primary btn-block',
                                                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>',
                                                'browseLabel' =>  'Выберите изображение'
                                            ],
                                            'options' => ['accept' => 'image/*']
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <h4>Блок с заголовоком и текстом</h4>
                                <?= $form->field($modelContent, "[{$i}]content1_header")->textInput() ?>
                                <?= $form->field($modelContent, "[{$i}]content1_text")->widget(CKEditor::class) ?>
                                <h4>Серый блок</h4>
                                <?= $form->field($modelContent, "[{$i}]content2")->widget(CKEditor::class) ?>
                                <h4>Серый блок с человеком</h4>
                                <?= $form->field($modelContent, "[{$i}]content3")->widget(CKEditor::class) ?>
                                <?= $form->field($modelContent, "[{$i}]content3_human_name")->textInput() ?>
                                <?= $form->field($modelContent, "[{$i}]content3_human_site")->textInput() ?>
                                <?= $form->field($modelContent, "[{$i}]content3_human_site_link")->textInput() ?>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <?php if (!empty($modelContent->content3_human_image)) { ?>
                                            <?=Html::img($modelContent->content3_human_image, ['width' => 250])?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-9">
                                        <?=$form->field($modelContent, "[{$i}]content3_human_image")->widget(FileInput::className(), [
                                            'pluginOptions' => [
                                                'showCaption' => false,
                                                'showRemove' => false,
                                                'showUpload' => false,
                                                'browseClass' => 'btn btn-primary btn-block',
                                                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>',
                                                'browseLabel' =>  'Выберите изображение'
                                            ],
                                            'options' => ['accept' => 'image/*']
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="panel-heading" style="height: 45px;padding: 10px 0 0 0;">
                    <div class="pull-right">
                        <button type="button" class="add-content btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                    </div>
                </div>
            </div>
        </div>
    <?php DynamicFormWidget::end(); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
    $(".dynamicform_wrapper_content").on("afterInsert", function(e, item) {
        const id = $(item).find('[id*=-p]').attr('id');
        const content2 = $(item).find('[id*=-content2]').attr('id');
        const content3 = $(item).find('[id*=-content3]').attr('id');
        const content1_text = $(item).find('[id*=-content1_text]').attr('id');
        
        CKEDITOR.replace(id, {"on":{"instanceReady":function( ev ){mihaildev.ckEditor.registerOnChange(id);}}})
        CKEDITOR.replace(content2, {"on":{"instanceReady":function( ev ){mihaildev.ckEditor.registerOnChange(content2);}}})
        CKEDITOR.replace(content3, {"on":{"instanceReady":function( ev ){mihaildev.ckEditor.registerOnChange(content3);}}})
        CKEDITOR.replace(content1_text, {"on":{"instanceReady":function( ev ){mihaildev.ckEditor.registerOnChange(content1_text);}}})
    });
JS;
$this->registerJs($script);
?>


<style>
.file-preview  {
    display: none;
}
</style>