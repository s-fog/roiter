<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Human */

$this->title = 'Создание человека';
?>
<div class="human-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
