<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>

    <?=$this->render('@backend/views/_image', [
        'form' => $form,
        'model' => $model,
        'image' => $model->image,
        'name' => 'image'
    ])?>

    <?= $form->field($model, 'icon')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className()) ?>

    <?= $form->field($model, 'link')->textInput(['rows' => 6]) ?>

    <?= $form->field($model, 'button_text')->textInput(['maxlength' => true]) ?>

    <?=$this->render('@backend/views/_image', [
        'form' => $form,
        'model' => $model,
        'image' => $model->man_image,
        'name' => 'man_image'
    ])?>

    <?= $form->field($model, 'man_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'man_function')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
