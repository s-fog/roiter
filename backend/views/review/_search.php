<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ReviewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'header') ?>

    <?= $form->field($model, 'image') ?>

    <?= $form->field($model, 'icon') ?>

    <?= $form->field($model, 'man_image') ?>

    <?php // echo $form->field($model, 'man_name') ?>

    <?php // echo $form->field($model, 'man_function') ?>

    <?php // echo $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'link') ?>

    <?php // echo $form->field($model, 'button_text') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
