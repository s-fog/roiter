<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%articles}}`.
 */
class m200824_045242_create_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%articles}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'slug' => $this->string(),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
            'seo_title' => $this->string(),
            'seo_h1' => $this->string(),
            'seo_description' => $this->text(),
            'human_id' => $this->integer(),
            'category_id' => $this->integer(),
            'icon' => $this->text(),
            'text' => $this->text(),
            'image' => $this->string(),
            'feature1_red' => $this->string(),
            'feature1_text' => $this->string(),
            'feature2_red' => $this->string(),
            'feature2_text' => $this->string(),
            'feature3_red' => $this->string(),
            'feature3_text' => $this->string(),
            'visit_count' => $this->smallInteger()->unsigned(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%articles}}');
    }
}
