<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%reviews}}`.
 */
class m200822_164216_create_reviews_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%reviews}}', [
            'id' => $this->primaryKey(),
            'header' => $this->string(),
            'image' => $this->string(),
            'icon' => $this->text(),
            'man_image' => $this->string(),
            'man_name' => $this->string(),
            'man_function' => $this->string(),
            'text' => $this->text(),
            'link' => $this->text(),
            'button_text' => $this->string(),
            'sort_order' => $this->smallInteger()->unsigned(),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%reviews}}');
    }
}
