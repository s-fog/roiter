<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%content}}`.
 */
class m200824_050008_create_content_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%content}}', [
            'id' => $this->primaryKey(),
            'article_id' => $this->integer(),
            'p' => $this->text(),
            'image' => $this->string(),
            'content1_header' => $this->string(),
            'content1_text' => $this->text(),
            'content2' => $this->text(),
            'content3' => $this->text(),
            'content3_human_image' => $this->string(),
            'content3_human_name' => $this->string(),
            'content3_human_site' => $this->string(),
            'content3_human_site_link' => $this->text(),
            'sort_order' => $this->smallInteger()->unsigned(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%content}}');
    }
}
