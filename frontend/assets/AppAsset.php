<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'fontawesome-free-5.13.1-web/css/fontawesome.min.css',
        'fontawesome-free-5.13.1-web/css/solid.min.css',
        'fontawesome-free-5.13.1-web/css/brands.min.css',
        'css/fonts.css',
        'libs/bootstrap/bootstrap.css',
        'libs/aos/aos.css',
        'libs/slick/slick.css',
        'css/jquery.fancybox.min.css',
        'css/app.css',
    ];
    public $js = [
        'libs/popper/popper.min.js',
        'libs/bootstrap/bootstrap.min.js',
        'libs/slick/slick.min.js',
        'libs/aos/aos.js',
        'js/jquery.fancybox.min.js',
        'js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
