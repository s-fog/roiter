<section class="section section-cashier">
        <div class="carousel" data-items-xl="1" data-items-lg="1" data-items-md="1" data-items-sm="1">
            <div class="slider-item">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-xl-8">
                            <div class="c-left">
                                <div class="section-title section-title__white">
                                    <span>мы гордимся нашими проектами:</span>
                                    <p>Школа танцев Евгения Папунаишвили</p>
                                </div>
                                <div class="terminal">
                                    <img src="/img/shtep-ico.png" alt="">
                                    <p>
                                        Школа танцев ШТЕП
                                        <a href="https://www.tantci.ru" target="_blank" rel="nofollow">www.tantci.ru</a>
                                    </p>
                                </div>
                                <div class="v">
                                    <span class="v__left">Евгений Папунаишвили</span>
                                    <span class="v__right">{идейный вдохновитель}</span>
                                </div>
                                <div class="vb">«Спасибо, что даете возможность заниматься моим любимым делом взяв на себя всю рутину с рекламой.» ©</div>
                                <div class="vbb articles__itemFeatures" style="max-width: 600px;">
                                    <div class="articles__itemFeature">
                                        <div>+410%</div>
                                        <span>прирост лидов <br> за период работы</span>
                                    </div>
                                    <div class="articles__itemFeature">
                                        <div>-42%</div>
                                        <span>стоимость <br> одного лида</span>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-red js-to-form" style="margin: 25px 0 0 0;">Хочу так же!</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="cashier-img">
                                <img src="/img/shtep.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="slider-item">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-xl-8">
                            <div class="c-left">
                                <div class="section-title section-title__white">
                                    <span>мы гордимся нашими проектами:</span>
                                    <p>Национальный Рекламный форум</p>
                                </div>
                                <div class="terminal">
                                    <img src="/img/nrf-ico.png" alt="">
                                    <p>
                                        НРФ
                                        <a href="https://advertisingforum.ru" target="_blank" rel="nofollow">www.advertisingforum.ru</a>
                                    </p>
                                </div>
                                <div class="v">
                                    <span class="v__left">Смоляков Валентин</span>
                                    <span class="v__right">{глава оргкомитета}</span>
                                </div>
                                <div class="vb">«Компания ROITER зарекомендовала себя как профессиональная команда, способная оперативно выполнять работу различной сложности.» ©</div>
                                <div class="vbb articles__itemFeatures" style="max-width: 600px;">
                                    <div class="articles__itemFeature">
                                        <div>+720%</div>
                                        <span>возврат <br> инвестиций (ROI)</span>
                                    </div>
                                    <div class="articles__itemFeature">
                                        <div>3</div>
                                        <span>источника <br> трафика</span>
                                    </div> 
                                </div>
                                <a href="#" class="btn btn-red js-to-form" style="margin: 25px 0 0 0;">Хочу так же!</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="cashier-img">
                                <img src="/img/smolyakov.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="slider-item">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-xl-8">
                            <div class="c-left">
                                <div class="section-title section-title__white">
                                    <span>мы гордимся нашими проектами:</span>
                                    <p>Система <br> Электронный кассир</p>
                                </div>
                                <div class="terminal">
                                    <img src="/img/terminal-ico.png" alt="">
                                    <p>
                                        Терминалы самообслуживания
                                        <a href="https://el-terminal.ru/" target="_blank" rel="nofollow">www.el-terminal.ru</a>
                                    </p>
                                </div>
                                <div class="v">
                                    <span class="v__left">Янковский Андрей</span>
                                    <span class="v__right">{директор по развитию}</span>
                                </div>
                                <div class="vb">«Выражаю благодарность команде ROITER. Рад, что у нас получилось выстроить отлаженный механизм привлечения клиентов.» ©</div>
                                <div class="vbb articles__itemFeatures" style="max-width: 600px;">
                                    <div class="articles__itemFeature">
                                        <div>+35%</div>
                                        <span>прирост заявок <br> с сайта</span>
                                    </div>
                                    <div class="articles__itemFeature">
                                        <div>+17%</div>
                                        <span>рост <br> качества трафика</span>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-red js-to-form" style="margin: 25px 0 0 0;">Хочу так же!</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="cashier-img">
                                <img src="/img/cashier.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
        </div>
    </section>