<?php

use frontend\models\ContactForm;
use yii\widgets\ActiveForm;
?>

<div class="col-md-6 col-lg-7 col-xl-8 form-column">
    <div class="section-title">
        <p>Напишите нам</p>
    </div>
    <?php
    $contactForm = new ContactForm();
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'sendForm'
        ],
    ]);?>
        <div class="row">
            <div class="col-xl-6">
                <?=$form->field($contactForm, 'name')
                    ->textInput([
                        'placeholder' => 'Ваше имя'
                    ])->label(false)?>
            </div>
            <div class="col-xl-6">
                <?=$form->field($contactForm, 'email')
                    ->textInput([
                        'placeholder' => 'Ваш Email*'
                    ])->label(false)?>
            </div>
            <div class="col-xl-12">
                <?=$form->field($contactForm, 'message')
                    ->textInput([
                        'placeholder' => 'Ваше сообщение'
                    ])->label(false)?>
            </div>
        </div>
        <button type="submit"
            data-text="Отправить сообщение"
            class="btn btn-red js-button">ОТПРАВИТЬ СООБЩЕНИЕ</button>
        <div class="policy">
            <svg class="lock-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.65 18.22"><g data-name="Слой 2"><path d="M12.61 7.44V5.29A5.29 5.29 0 102 5.29v2.15H0v10.78h14.65V7.44zM4.15 5.29a3.18 3.18 0 016.35 0v2.15H4.15z" fill="#878888" data-name="Слой 1"/></g></svg>
            Заполняя данную форму, я даю согласие на обработку моих персональных данных.
        </div>
        <?=$form->field($contactForm, 'type')
            ->hiddenInput([
                'value' => 'Отправлена форма с сайта Roiter'
            ])->label(false)?>

        <?=$form->field($contactForm, 'BC')
            ->textInput([
                'class' => 'BC',
                'value' => ''
            ])->label(false)?>
    <?php ActiveForm::end();?>
</div>