<?php
use frontend\models\ContactForm3;
use yii\widgets\ActiveForm;
?>

<section class="section section-contacts" id="form">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-lg-6 col-xl-6">
                <div class="image-before">
                <span>Α/Ω</span>
                <img src="/img/worker.png" alt="">
                </div>
            </div>
            <div class="col-md-5 col-lg-6 col-xl-6">
                <div class="section-title">
                <span>Напишите, и мы</span>
                <p>Приведем клиентов<br/> в ваш бизнес</p>
                </div>
                <div class="contact-form">
                <?php 
                    $contactForm = new ContactForm3();
                    $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'sendForm'
                        ],
                    ]);
                ?>
                    <?=$form->field($contactForm, 'name', [
                        'template' => '<span><i class="fas fa-user-alt"></i></span>{input}{error}',
                    ])
                        ->textInput([
                            'placeholder' => 'Ваше имя'
                        ])->label(false)?>
                    <?=$form->field($contactForm, 'email', [
                        'template' => '<span><i class="fas fa-envelope"></i></span>{input}{error}',
                    ])
                        ->textInput([
                            'placeholder' => 'Ваш e-mail'
                        ])->label(false)?>
                    <?=$form->field($contactForm, 'site', [
                        'template' => '<span><i class="fas fa-phone"></i></span>{input}{error}',
                    ])
                        ->textInput([
                            'placeholder' => 'Ваш телефон'
                        ])->label(false)?>
                 
                 <!--   <?=$form->field($contactForm, 'message', [
                        'template' => '<span><i class="fas fa-globe"></i></span>{input}{error}',
                    ])
                        ->textInput([
                            'placeholder' => 'Ваш сайт'
                        ])->label(false)?>  -->
                        
                    <button type="submit" 
                        data-text="Отправить заявку"
                        class="btn btn-red js-button">
                    отправить заявку
                    </button>
                    <div class="policy">
                    <svg class="lock-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.65 18.22"><g data-name="Слой 2"><path d="M12.61 7.44V5.29A5.29 5.29 0 102 5.29v2.15H0v10.78h14.65V7.44zM4.15 5.29a3.18 3.18 0 016.35 0v2.15H4.15z" fill="#878888" data-name="Слой 1"/></g></svg>
                    Заполняя данную форму, я даю согласие на обработку моих персональных данных.
                    </div>
                    <?=$form->field($contactForm, 'type')
                        ->hiddenInput([
                            'value' => 'Отправлена форма с сайта Roiter'
                        ])->label(false)?>

                    <?=$form->field($contactForm, 'BC')
                        ->textInput([
                            'class' => 'BC',
                            'value' => ''
                        ])->label(false)?>
                <?php ActiveForm::end();?>
                </div>
            </div>
        </div>
    </div>
</section>