<section class="section section-recommend">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-5 col-xl-5 image">
                    <img src="/img/paper.png" alt="">
                </div>
                <div class="col-md-12 col-lg-7 col-xl-7">
                    <div class="right-side">
                        <div class="section-title section-title__white">
                            <p>Нас рекомендует</p>
                        </div>
                        <div class="description">
                            АКАР – крупнейшее профессиональное <br/>
                            объединение более 200 участников рынка <br/>
                            коммуникационных услуг России.
                        </div>
                        <a href="/otzyv-o-roiter.pdf" target="_blank" class="btn btn-red__bordered">
                            смотреть рекомендацию
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>