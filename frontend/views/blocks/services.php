<section class="section section-price">
        <div class="container">
            <div class="price-elem">
                <div class="row flex">
                    <div class="col-sm-12 col-md-5 col-lg-6 col-xl-6 flex-middle">
                        <img src="/img/price-img1.png" alt="">
                    </div>
                    <div class="col-sm-12 col-md-7 col-lg-6 col-xl-6 flex-middle info">
                        <div class="section-title">
                            <span>Если необходимо</span>
                            <p>Привести клиентов</p>
                        </div>
                        <ul class="price-list">
                            <li><a href="/context">Контекстная реклама</a></li>
                            <li>Продвижение сайта (SEO)</li>
                            <li>Медийная реклама</li>
                            <li>Таргетированная реклама</li>
                            <li>Influence Marketing</li>
                            <li>Комплексное продвижение</li>
                        </ul>
                        <div class="price-btns">
                            <a href="#" class="btn btn-red js-to-form">заказать</a>
                            <a href="/context" class="btn btn-gray__bordered">подробнее</a>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="price-elem">
                <div class="row flex">
                    <div class="col-sm-12 col-md-7 col-lg-6 col-xl-6 flex-middle info">
                        <div class="section-title">
                            <span>Если необходимо</span>
                            <p>Увеличить конверсию</p>
                        </div>
                        <ul class="price-list">
                            <li>Аудит UX/UI</li>
                            <li>Онлайн-аналитика</li>
                            <li>Аудит рекламных размещений</li>
                        </ul>
                        <div class="price-btns">
                            <a href="#" class="btn btn-red js-to-form">заказать</a>
                            <!--<a href="#" class="btn btn-gray__bordered">подробнее</a>-->
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-5 col-lg-6 col-xl-6 flex-middle">
                        <img src="/img/price-img2.png" alt="">
                    </div>
                </div>
            </div>
            
        </div>
    </section>