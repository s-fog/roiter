<?php

use common\models\Article;
use common\models\Category;
use yii\widgets\LinkPager;

if (isset($model)) {
	$this->params['seotitle'] = $model->seo_title;
	$this->params['seodescription'] = $model->seo_description;
} else {
	$this->params['seotitle'] = 'Интересное | Roiter';
	$this->params['seodescription'] = 'Собрали для вас все самое интересное! Кейсы, статьи, новости и многое другое!';
}

if (!isset($articles)) {
	list($articles, $pages) = Article::getItems();
}

?>
<section class="content">
	<div class="breadcrumbs">
		<div class="container">
			<ul>
				<li>
					<a href="/">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.84 17"><g data-name="Слой 2"><path fill="#797979" d="M10.92 0L0 8.83h3V17h5.91v-6.61h4.17V17h5.77V8.83h2.99L10.92 0z" data-name="Слой 1"/></g></svg>
						Главная
					</a>
				</li>
				<li>
					<p>></p>
				</li>
				<li>
					<span><?=isset($model) ? $model->name : 'Интересное' ?></span>
				</li>
			</ul>
		</div>
	</div>
	<section class="section-headers">
		<div class="container">
			<span><?=!empty($model->near_h1) ? $model->near_h1 : 'мы собрали всё самое'?></span>
			<?php
				if (isset($model)) {
					$h1 = !empty($model->seo_h1) ? $model->seo_h1 : $model->name;
				} else {
					$h1 = 'интересное';
				}
			?>
			<p><?=$h1?></p>
		</div>
	</section>
	<section class="container container-900">
		<div class="articleCategories">
		<?php 
			$categories = Category::find()->orderBy('sort_order', SORT_DESC)->all();

			foreach($categories as $category) {
				$active = false;

				if (isset($model)) {
					if ($model->id == $category->id) {
						$active = true;
					}
				}
		?>
			<a href="<?=$category->url?>" class="articleCategories__item<?= $active ? ' active' : '' ?>">#<?=$category->name?></a>
		<?php 
			} 
		?>
		<!-- <a href="/<?=Yii::$app->params['blogAlias']?>" class="articleCategories__item">показать все</a> -->
		</div>
		<div class="articles">
			<?php foreach($articles as $article) { ?>
				<?=$this->render('_article', ['model' => $article])?>
			<?php } ?>
		</div>
		<?=LinkPager::widget([
			'pagination' => $pages,
			'disableCurrentPageButton' => true,
			'hideOnSinglePage' => true,
			'maxButtonCount' => 6,
			'nextPageLabel' => '&gt;',
			'prevPageLabel' => '&lt;',
		]);?>
	</section>
    <?=$this->render('@frontend/views/blocks/form3')?>
</section>