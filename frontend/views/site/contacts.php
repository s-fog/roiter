<?php
$this->params['seotitle'] = 'Контакты | ROITER';
$this->params['seodescription'] = 'Контактые телефоны, электронная почта, адрес и все, что может вам пригодиться для связи с агентством Ройтер.';
?>
<section class="content">
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li>
                    <a href="/">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.84 17"><g data-name="Слой 2"><path fill="#797979" d="M10.92 0L0 8.83h3V17h5.91v-6.61h4.17V17h5.77V8.83h2.99L10.92 0z" data-name="Слой 1"/></g></svg>
                        Главная
                    </a>
                </li>
                <li>
                    <p>></p>
                </li>
                <li>
                    <span>Контакты</span>
                </li>
            </ul>
        </div>
    </div>
    <section class="section section-contact">
        <div class="map">
            <div id="map" style="width: 100%;position: absolute;height: 100%;"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-5 col-xl-4">
                    <div class="section-title">
                        <p>Контакты</p>
                    </div>
                    <ul>
                        <li>
                          <div class="circle">
                            <i class="fas fa-phone"></i>
                          </div>
                            <div class="contact-info">
                                <a href="mailto:hello@roiter.ru">hello@roiter.ru</a>
                                <a href="tal:88005004519">8 800 500 45 19</a>
                            </div>
                        </li>
                        <li>
                          <div class="circle">
                            <i class="fas fa-map-marker-alt"></i>
                          </div>
                            <div class="contact-info">
                                <p>Московская Область, Видное,<br/> ул. Олимпийская, д.6, корп. 1</p>
                            </div>
                        </li>
                        <li>
                          <div class="circle">
                            <i class="fas fa-clock"></i>
                          </div>
                            <div class="contact-info">
                                <p>Пн. - Сб. : 9:00 - 21:00<br/> Воскресение: выходной</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <?=$this->render('@frontend/views/blocks/form1')?>
            </div>
        </div>
    </section>
</section>

<script>
    function initMap() {
  var myLatLng = {lat: 55.545936, lng: 37.699127};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    disableDefaultUI: true,
    styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }
],
    center: myLatLng,
    scrollwheel: false
  });
  var svg = '<svg style="width: 10px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 238.5 328.7"><path d="M87.8 219c-.4-.6-.7-1.2-1-1.7l-27-43.2c-5.4-8.7-10.9-17.3-16.2-26-5.4-8.9-8.9-18.6-10.9-28.8-1.2-6-1.7-12.1-1.5-18.2.6-20.9 7.3-39.6 20.9-55.6 7-8.2 15.1-15.1 24.5-20.5 8.2-4.7 17-7.8 26.2-9.7 3.5-.7 7.1-1.1 10.7-1.6 1.8-.2 3.6-.2 5.4-.3h2.5c2.8-.1 5.5.1 8.2.5 3.7.5 7.3 1 10.9 1.8 15 3.4 28.4 10.1 39.7 20.6 9.9 9.1 17.7 19.8 23 32.3 2.8 6.7 4.7 13.6 5.8 20.7.7 5.1 1 10.2.9 15.3-.1 4.3-.5 8.6-1.2 12.9-1.7 10.7-5.3 20.7-10.9 30-25.3 42.1-50.7 84.2-76.1 126.4-.4.6-.7 1.1-1.1 1.7-.4-.1-.5-.4-.6-.6-2.3-3.7-4.5-7.3-6.8-11-3.4-5.6-6.8-11.2-10.3-16.8-.2-.2-.3-.5-.5-.7-.4-.6-1-.8-1.7-.7-2.3.3-4.7.6-7 .9-2.9.4-5.8.8-8.6 1.2-10.7 1.7-21.1 4.4-30.9 9-2.3 1.1-4.6 2.2-6.5 3.9-.5.4-1 .8-1.4 1.4.7.6 1.4 1.2 2.1 1.7 2.5 1.9 5.3 3.3 8.2 4.5 5.8 2.4 11.7 4.2 17.8 5.6 5.8 1.4 11.7 2.4 17.6 3.1 3.6.4 7.1.8 10.7 1 6.4.5 12.8.7 19.2.7 3.9 0 7.9-.1 11.8-.3 5.8-.3 11.6-.8 17.3-1.7 8.9-1.3 17.8-3 26.4-5.8 4.2-1.4 8.3-3 12.2-5.1 1.9-1 3.6-2.2 5.1-3.7.1-.1.2-.2.3-.4-.2-.3-.4-.4-.7-.6-5.3-3.6-11-6.2-17-8.2-5.6-1.9-11.4-3.3-17.2-4.6-.9-.2-1.9-.4-2.8-.6-.2 0-.4 0-.6-.2 0-.3.2-.5.3-.7l14.4-23.1c.5-.9 1.2-1.1 2.1-.9 7.7 1.8 15.3 4.1 22.5 7.3 4.6 2.1 9.1 4.5 13.2 7.5 3.4 2.6 6.5 5.5 8.9 9 2.6 3.7 4.3 7.8 4.8 12.3.7 5.7-.5 11-3.2 15.9-2 3.5-4.6 6.4-7.6 9-4.9 4.3-10.4 7.5-16.3 10.1-5.7 2.5-11.6 4.4-17.6 6-6.5 1.7-13.1 3.1-19.8 4.1-3.6.5-7.2 1-10.8 1.3-5 .5-10 .8-15 1.1-2.4.2-4.7.2-7.1.2h-13c-.9 0-1.7-.2-2.5-.3-1.9-.1-3.7-.1-5.6-.2-3.6-.2-7.2-.5-10.8-.9-4.2-.4-8.3-.9-12.5-1.6-10.7-1.7-21.3-4.1-31.5-8-5.3-2.1-10.4-4.5-15.1-7.6-4-2.6-7.6-5.7-10.5-9.4-3.1-3.9-5.1-8.3-5.8-13.3-.7-5.5.3-10.7 3-15.5 2.4-4.4 5.7-8 9.6-11.1 4.1-3.3 8.7-5.9 13.4-8.2 7.1-3.5 14.4-6 22.1-7.8 5.5-1.4 11.1-2.4 16.7-3.2 1.5-.1 3.1-.4 4.8-.6z" fill="#e5273c"/><path d="M167.4 147.9c-.3.1-.6.1-.9.1h-19.9c-.7 0-1-.2-1.3-.8-3.3-6.1-6.6-12.1-10-18.2-2.3-4.1-4.6-8.3-6.8-12.4-.2-.4-.5-.6-1-.6h-24.6c-.3 0-.5.1-.5.4v30.5c0 1.1.1 1-1 1H83.7c-.3 0-.4-.1-.4-.4V62c0-1-.1-.9.9-.9h47.3c4.5 0 8.9.5 13.1 1.7 4.8 1.4 9.1 3.8 12.6 7.4 4 4.1 6.5 9 7.2 14.7.7 5.3-.1 10.4-2.6 15.1-1.9 3.7-4.7 6.7-8.1 9.1-1.9 1.3-3.8 2.5-5.9 3.4 0 0-.1 0-.1.1-.6.3-.6.3-.3.8 1 1.7 1.9 3.3 2.9 5 5.6 9.6 11.1 19.2 16.7 28.7.3.4.4.6.4.8z" fill="#0e1218"/><path d="M118.9 100h-15.8c-.8 0-.8 0-.8-.8V77.8c0-.6 0-.6.6-.6h30c2 0 4.1.2 5.9 1.1 3.9 1.9 6.4 5 6.6 9.4.3 4.8-1.8 8.3-5.9 10.7-1.9 1.1-3.9 1.5-6 1.5-4.8 0-9.7.1-14.6.1z" fill="#e6273c"/></svg>';
  
  var iconBase = 'img/marker.png';
  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    draggable:false,
    animation: google.maps.Animation.DROP,
    icon: {
      url: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg),
      scaledSize: new google.maps.Size(55, 120)
    }
  });

};
</script>

<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBLmSrYz01qDh9um3Bp6v6995mt43clmh0&#038;callback=initMap'></script>