<?php

use frontend\models\ContactForm3;
use yii\bootstrap\ActiveForm;

$this->params['seotitle'] = 'Таргетированная реклама в соц. сетях | Roiter';
$this->params['seodescription'] = 'Настройка и ведение таргетированной рекламы с упором на финансовую эффективность вложений. Находимся в топе лучших агентств таргетированной рекламы Москвы.';
?>
<section class="content">
  <div class="breadcrumbs">
    <div class="container">
      <ul>
        <li>
          <a href="/">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.84 17"><g data-name="Слой 2"><path fill="#797979" d="M10.92 0L0 8.83h3V17h5.91v-6.61h4.17V17h5.77V8.83h2.99L10.92 0z" data-name="Слой 1"/></g></svg>
            Главная
          </a>
        </li>
        <li>
          <p>></p>
        </li>
        <li>
          <a href="/services">
            Услуги
          </a>
        </li>
        <li>
          <p>></p>
        </li>
        <li>
          <span>Таргетированная реклама</span>
        </li>
      </ul>
    </div>
  </div>
  <section class="section section-main">
    <div class="container">
      <p class="main-title">Таргетированная реклама<br/>в социальных сетях</p>
      <div class="descr">
        
        Получите новых клиентов уже на следующий день после запуска,<br/> благодаря проверенной на нескольких сотнях проектах технологии<br/> настройки и управления рекламой от наших специалистов.
      </div>
      <a href="#" class="btn btn-red js-to-form">
        ОТПРАВИТЬ ЗАЯВКУ
      </a>
      <img src="img/main-img.png" class="right-image"  alt="">
    </div>
  </section>
  
  
  
  <section class="section section-advantages">
    <div class="container">
      <div class="section-title">
        <span>вас удивят</span>
        <p>Преимущества рекламы в социальных сетях</p>
      </div>
      <div class="row">
        <div class="col-lg-4 col-xl-4">
          <div class="a-elem">
            <div class="a-img">
              <svg style="fill: #E6273C;width: 57px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 692.5 692.5" xml:space="preserve"><style>.st0{fill:#fff}</style><path class="st0" d="M0 0h692.5v692.5H0V0zm278.5 338.8c-.5-.3-.7-.4-.8-.5-3.5-3.2-7.5-4-11.8-2.1-4.4 2-6.4 5.6-6.4 10.4 0 6.9-.2 13.8.1 20.8.1 2.3 1.2 4.7 2.2 6.8.5 1.1.9 1.7.2 2.8-9.1 16.9-18 33.9-27.2 50.9-5.7 10.5-14.5 16.3-26.5 17.3-9.8.8-19.7 1.5-29.5 2.2-14.8 1.1-29.6 2.2-44.4 3.4-4.9.4-9.8.7-14.5 1.9-18.8 5-30.7 23.6-27.8 43.1 2.7 18.8 19.3 33 38.5 32.7 6-.1 12.1-.6 18.1-1 16.4-1.1 32.8-2.2 49.2-3.2 15-1 30-1.5 44.9-3.1 27.6-2.8 50.2-15.5 67-37.5 13.3-17.3 25.9-35.1 38.8-52.7l1.8-2.4c.5.6.9 1.1 1.3 1.5 15.1 18 30 36.1 45.3 54 9.9 11.6 17.8 24.1 22.6 38.7 10.5 32.5 21.3 64.9 31.9 97.4 5.4 16.6 16.7 26.1 34 28.4 25 3.3 46.9-19.4 42.8-44.4-3.7-22.3-7.4-44.6-11-66.9-4.7-29-14.5-56.1-29.9-81.1-13.9-22.6-28.1-45.1-42.2-67.6-2.4-3.8-4.8-7.7-7.3-11.6 4.5-2.8 5.6-6.9 5.4-11.8-.2-5.5-.2-11 0-16.4.2-5.2-1.2-9.5-6.1-12.2 4.4-20.3 8.8-40.4 13.2-60.9.8.6 1.3.9 1.9 1.4 7.4 6 14.7 12.1 22.2 18 16.1 12.7 33.9 15.6 53.3 8.9 7.1-2.5 13.9-5.5 20.9-8.3 15.5-6.2 31.1-12.2 46.5-18.7 16.5-6.9 24.6-25.6 19-42.6-5.6-17-23.2-26.9-40.7-22.8-17.1 4.1-34.2 8.5-51.3 12.8-4.5 1.1-8.2.1-11.5-3.1-3.7-3.7-7.4-7.3-11-11-10.3-10.3-21.5-19.4-33.9-27-30.7-18.9-64.5-26.8-100.1-27.9-15.7-.5-31.2 1.3-46.7 3.5-19.7 2.9-39.5 5.7-59.2 8.6-19.8 2.9-34.5 13.2-42.8 31.5-10.1 22.4-19.7 45.1-29.1 67.8-5.8 13.9-.9 30.6 10.9 39.7 16.7 12.9 40.2 8.4 51.2-9.8 9-14.9 17.9-29.7 26.7-44.7 2.5-4.2 6-6.4 10.9-6.6 4.7-.2 9.4-.5 14-.8.2 0 .5.1.9.2-8.1 31.3-16 62.6-24 94zM410.8 21.6c-35.7 0-64.7 29.1-64.8 64.8 0 35.8 29.1 64.9 64.9 64.9 35.7 0 64.9-29.2 64.9-64.9-.1-35.8-29.2-64.8-65-64.8zM97.3 356.8v10.8c.1 6.1 4.8 10.9 10.9 10.8 5.9 0 10.7-4.7 10.7-10.7.1-7.3.1-14.5 0-21.8-.1-6-4.8-10.7-10.7-10.7-6 0-10.8 4.7-10.9 10.8v10.8zm54.1-.1v10.6c.1 6.3 4.7 11.1 10.8 11.1 6.1 0 10.8-4.7 10.8-11.1v-21.2c0-6.3-4.8-11-10.9-11-6 0-10.7 4.7-10.7 10.9-.1 3.7 0 7.2 0 10.7zm54 .1v10.8c.1 6.1 4.9 10.8 10.9 10.8 5.9 0 10.7-4.7 10.7-10.7.1-7.3.1-14.5 0-21.8-.1-6-4.8-10.7-10.7-10.7-6 0-10.8 4.7-10.8 10.8-.1 3.6-.1 7.2-.1 10.8zm292 .1v-10.8c-.1-6.2-4.8-10.9-10.8-10.8-6 0-10.7 4.7-10.8 10.9-.1 7.1-.1 14.3 0 21.4 0 6.2 4.7 10.9 10.7 11 6.1 0 10.8-4.7 10.9-11v-10.7zm32.4-.2v10.8c.1 6.2 4.8 10.9 10.7 10.9 6 0 10.8-4.7 10.8-10.8.1-7.2.1-14.4 0-21.6-.1-6.1-4.8-10.8-10.9-10.8-6 0-10.7 4.7-10.7 10.9.1 3.6.1 7.1.1 10.6zm54.1.1v10.8c.1 6.1 4.9 10.8 10.9 10.8 5.9 0 10.7-4.7 10.7-10.7.1-7.3.1-14.5 0-21.8-.1-6-4.8-10.7-10.7-10.7-6 0-10.8 4.7-10.8 10.8-.1 3.6-.1 7.2-.1 10.8z"/><path d="M278.5 338.8c8-31.4 16-62.7 23.9-94-.4-.1-.6-.3-.9-.2-4.7.2-9.4.6-14 .8-4.9.2-8.3 2.4-10.9 6.6-8.8 14.9-17.8 29.8-26.7 44.7-11 18.2-34.4 22.6-51.2 9.8-11.8-9.1-16.7-25.8-10.9-39.7 9.5-22.7 19-45.4 29.1-67.8 8.2-18.2 23-28.5 42.8-31.5 19.7-2.9 39.5-5.7 59.2-8.6 15.5-2.3 31-4 46.7-3.5 35.7 1.1 69.4 9 100.1 27.9 12.4 7.6 23.7 16.7 33.9 27 3.7 3.7 7.4 7.3 11 11 3.2 3.2 7 4.2 11.5 3.1 17.1-4.3 34.2-8.7 51.3-12.8 17.4-4.2 35.1 5.8 40.7 22.8 5.6 17-2.6 35.6-19 42.6-15.4 6.5-31 12.5-46.5 18.7-6.9 2.8-13.8 5.8-20.9 8.3-19.3 6.8-37.2 3.8-53.3-8.9-7.5-5.9-14.8-12-22.2-18-.5-.4-1.1-.8-1.9-1.4-4.4 20.4-8.8 40.6-13.2 60.9 4.9 2.7 6.3 6.9 6.1 12.2-.2 5.5-.2 11 0 16.4.2 4.9-1 9-5.4 11.8 2.5 4 4.9 7.8 7.3 11.6 14.1 22.5 28.3 45 42.2 67.6 15.4 25 25.2 52.2 29.9 81.1 3.6 22.3 7.3 44.6 11 66.9 4.1 24.9-17.8 47.7-42.8 44.4-17.3-2.3-28.6-11.8-34-28.4-10.6-32.5-21.4-64.9-31.9-97.4-4.7-14.6-12.7-27.1-22.6-38.7-15.3-17.9-30.2-36-45.3-54-.4-.4-.8-.9-1.3-1.5-.7.9-1.3 1.6-1.8 2.4-12.9 17.6-25.6 35.4-38.8 52.7-16.9 22-39.4 34.7-67 37.5-14.9 1.5-29.9 2.1-44.9 3.1-16.4 1.1-32.8 2.2-49.2 3.2-6 .4-12.1.9-18.1 1-19.2.3-35.8-13.9-38.5-32.7-2.8-19.5 9-38.1 27.8-43.1 4.7-1.3 9.7-1.5 14.5-1.9 14.8-1.2 29.6-2.3 44.4-3.4 9.8-.7 19.7-1.4 29.5-2.2 11.9-1 20.8-6.8 26.5-17.3 9.2-16.9 18.1-33.9 27.2-50.9.6-1.1.3-1.8-.2-2.8-1-2.1-2.1-4.5-2.2-6.8-.3-6.9-.1-13.8-.1-20.8 0-4.8 2-8.4 6.4-10.4 4.3-2 8.3-1.1 11.8 2.1.2.1.4.2.9.5zm82-161.9c-17.8-.3-35.1 3.1-52.6 5.5-14.6 2-29.1 4.3-43.6 6.2-13.8 1.8-23.5 8.7-28.8 21.6-3.2 7.8-6.6 15.5-9.9 23.2-5.8 13.4-11.6 26.8-17.3 40.3-4.1 9.6 3.1 19.3 13.4 18.3 4.6-.5 7.7-3.1 10-7 8.8-14.8 17.7-29.5 26.5-44.3 5.4-9.1 13.2-14.9 23.7-16.4 6-.9 12.1-.9 18.1-1.3 16.5-1 33.1-2 49.6-2.9 6.3-.3 11.3 4.1 11.6 10 .4 6.2-4.1 11.1-10.6 11.6-7.6.5-15.2 1-22.9 1.3-2.1.1-2.9.8-3.4 2.8-9.5 37.6-19.1 75.2-28.7 112.8-.4 1.4-.9 2.9-1.6 4.2-13.3 25-26.5 50.1-40 75.1-9.3 17.2-23.8 27-43.2 28.8-27 2.4-54 4.1-81 6.2-7.5.6-13 4.3-15.5 11.5-2.4 6.9-.7 13.1 4.6 18.1 4.2 4 9.3 4.8 14.9 4.4 5.4-.5 10.8-.8 16.2-1.1 28.8-1.9 57.7-3.7 86.5-5.6 24.1-1.6 43.4-12.1 57.7-31.5 15.5-21 30.9-42.2 46.4-63.2 5-6.8 12.8-7 18.3-.5 18.4 21.9 36.6 44 55.2 65.8 11.6 13.5 20.6 28.4 26.1 45.3 10.6 32.7 21.4 65.4 32.2 98 .9 2.7 2.3 5.5 4.2 7.7 5.2 6 14 7.3 21.4 3.6 7-3.5 10.2-10.8 8.8-19.3-3.3-19.9-6.7-39.8-9.8-59.7-3.1-20-8.4-39.4-16.8-57.9-6.1-13.4-14.1-25.8-21.9-38.2l-47.4-75.6c-1.9-3-2.7-6-2-9.4 8-36.7 16.1-73.4 23.8-110.2 1.9-9 11.6-12.7 18.9-6.5 11.9 10.1 24.1 19.7 36.2 29.5 10.3 8.3 21.6 10 33.9 5.2 7-2.7 13.9-5.5 20.8-8.3 14.6-5.8 29.1-11.6 43.7-17.5 9.3-3.8 11-16.1 3.1-22.3-3.8-2.9-8-3.2-12.5-2.1-16.8 4.2-33.6 8.5-50.5 12.6-9.5 2.3-18.5.8-26.2-5.2-5.2-4-9.7-8.8-14.3-13.5-14.6-15-31.4-26.9-50.6-35.2-23.8-10.7-48.9-14.7-74.7-14.9zm50.3-155.3c35.8 0 64.9 29 65 64.8 0 35.7-29.1 64.9-64.9 64.9-35.7 0-64.9-29.2-64.9-64.9.1-35.7 29.1-64.7 64.8-64.8zm.1 19.2c-25.1 0-45.6 20.4-45.7 45.6-.1 25.1 20.4 45.7 45.5 45.8 25.1.1 45.9-20.6 45.9-45.7 0-25.2-20.5-45.7-45.7-45.7zm-313.6 316V346c.1-6.1 4.8-10.8 10.9-10.8 5.9 0 10.7 4.7 10.7 10.7.1 7.3.1 14.5 0 21.8-.1 6-4.8 10.7-10.7 10.7-6 0-10.8-4.7-10.9-10.8v-10.8z"/><path d="M151.4 356.7v-10.6c.1-6.2 4.7-10.9 10.7-10.9 6.1 0 10.8 4.7 10.9 11v21.2c0 6.3-4.7 11.1-10.8 11.1-6.1 0-10.7-4.8-10.8-11.1v-10.7zm54 .1V346c.1-6.1 4.8-10.8 10.8-10.8 5.9 0 10.7 4.7 10.7 10.7.1 7.3.1 14.5 0 21.8-.1 6-4.8 10.7-10.7 10.7-6 0-10.8-4.7-10.9-10.8.1-3.6.1-7.2.1-10.8zm292 .1v10.6c-.1 6.3-4.8 11-10.9 11-6 0-10.7-4.8-10.7-11 0-7.1-.1-14.3 0-21.4 0-6.2 4.8-10.9 10.8-10.9 6 0 10.7 4.7 10.8 10.8v10.9zm32.4-.2v-10.6c.1-6.2 4.7-10.9 10.7-10.9 6 0 10.8 4.7 10.9 10.8.1 7.2.1 14.4 0 21.6-.1 6.1-4.8 10.9-10.8 10.8-6 0-10.7-4.8-10.7-10.9-.1-3.6-.1-7.2-.1-10.8zm54.1.1V346c.1-6.1 4.8-10.8 10.8-10.8 5.9 0 10.7 4.7 10.7 10.7.1 7.3.1 14.5 0 21.8-.1 6-4.8 10.7-10.7 10.7-6 0-10.8-4.7-10.9-10.8.1-3.6.1-7.2.1-10.8z"/><path class="st0" d="M360.5 176.9c25.8.3 50.9 4.3 74.7 14.7 19.2 8.4 35.9 20.3 50.6 35.2 4.6 4.7 9.1 9.5 14.3 13.5 7.7 5.9 16.8 7.5 26.2 5.2 16.9-4.1 33.7-8.3 50.5-12.6 4.5-1.1 8.7-.9 12.5 2.1 7.9 6.2 6.2 18.5-3.1 22.3-14.5 5.9-29.1 11.7-43.7 17.5-6.9 2.8-13.9 5.6-20.8 8.3-12.3 4.8-23.6 3.1-33.9-5.2-12.1-9.8-24.3-19.4-36.2-29.5-7.2-6.1-17-2.5-18.9 6.5-7.7 36.8-15.9 73.4-23.8 110.2-.7 3.4.1 6.5 2 9.4l47.4 75.6c7.8 12.4 15.8 24.8 21.9 38.2 8.4 18.5 13.7 37.8 16.8 57.9 3.1 19.9 6.5 39.8 9.8 59.7 1.4 8.5-1.8 15.8-8.8 19.3-7.4 3.7-16.2 2.4-21.4-3.6-1.9-2.2-3.3-4.9-4.2-7.7-10.8-32.6-21.6-65.3-32.2-98-5.5-17-14.6-31.8-26.1-45.3-18.6-21.8-36.8-43.8-55.2-65.8-5.4-6.5-13.3-6.3-18.3.5-15.5 21.1-30.9 42.2-46.4 63.2-14.3 19.4-33.6 29.9-57.7 31.5-28.8 1.9-57.7 3.7-86.5 5.6-5.4.4-10.8.7-16.2 1.1-5.5.5-10.7-.4-14.9-4.4-5.3-5-7-11.2-4.6-18.1 2.5-7.2 7.9-10.9 15.5-11.5 27-2.1 54.1-3.8 81-6.2 19.4-1.7 34-11.6 43.2-28.8 13.5-25 26.7-50 40-75.1.7-1.3 1.3-2.8 1.6-4.2 9.6-37.6 19.2-75.2 28.7-112.8.5-2 1.3-2.7 3.4-2.8 7.6-.3 15.3-.8 22.9-1.3 6.5-.4 10.9-5.4 10.6-11.6-.4-5.9-5.3-10.3-11.6-10-16.6.9-33.1 1.9-49.6 2.9-6 .4-12.1.4-18.1 1.3-10.5 1.5-18.3 7.3-23.7 16.4-8.8 14.8-17.7 29.5-26.5 44.3-2.3 3.9-5.5 6.5-10 7-10.4 1.1-17.5-8.6-13.4-18.3 5.7-13.5 11.5-26.9 17.3-40.3 3.3-7.7 6.7-15.4 9.9-23.2 5.4-12.9 15.1-19.8 28.8-21.6 14.6-1.9 29.1-4.2 43.6-6.2 17.5-2.3 34.8-5.6 52.6-5.3zm-25.3 180v-10.8c-.1-6.2-4.8-10.9-10.8-10.9-6 0-10.7 4.7-10.8 10.9-.1 7.1 0 14.3 0 21.4 0 6.2 4.7 10.9 10.7 11 6.1 0 10.8-4.7 10.9-11v-10.6zm54.1-.2c0-3.6.1-7.2 0-10.8-.1-6-4.8-10.7-10.7-10.7-6 0-10.8 4.7-10.9 10.8-.1 7.2-.1 14.4 0 21.6 0 6.1 4.8 10.9 10.8 10.9 6 0 10.7-4.7 10.8-10.9v-10.9zm21.6-315.9c25.1 0 45.6 20.5 45.7 45.7 0 25.2-20.7 45.8-45.9 45.7-25.1-.1-45.6-20.8-45.5-45.8.1-25.2 20.6-45.6 45.7-45.6z"/><path d="M335.2 356.9v10.6c-.1 6.3-4.8 11-10.9 11-6 0-10.7-4.8-10.7-11 0-7.1-.1-14.3 0-21.4 0-6.2 4.8-10.9 10.8-10.9 6 0 10.7 4.7 10.8 10.9v10.8zm54.1-.2v10.8c-.1 6.2-4.8 10.9-10.8 10.9-6 0-10.8-4.7-10.8-10.9-.1-7.2-.1-14.4 0-21.6.1-6.1 4.8-10.8 10.9-10.8 5.9 0 10.6 4.7 10.7 10.7v10.9z"/></svg>
            </div>
            <div class="a-info">
              <p>Универсальность</p>
              <span> 
              Продажи, трафик, узнаваемость, 
              охват или укрепление лояльности? 
              Достигайте любых целей в соц. сетях!
              </span>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-xl-4">
          <div class="a-elem">
            <div class="a-img">
              <svg style="fill: #E6273C;width: 57px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 579.2 511.9"><path d="M186.8 318.9c-3.9-.5-7.7-1-11.5-1.5-13.5-1.8-27-3.4-40.1-7.2-5.4-1.6-10.7-3.7-16-5.7-3.4-1.3-6.4-3.3-9.2-5.6-4.1-3.5-7-7.9-8.3-13.1-1-4-.4-7.8 2.4-10.8 5.9-6.4 8.1-14.2 9.2-22.5 1.6-12 3.2-23.9 4.7-35.9 1-8.2 1.7-16.4 2.6-24.6 2.4-23.2 5.4-46.2 11.5-68.8 4.2-15.4 9.7-30.3 17.8-44.1 10.1-17.2 23.2-31.5 40.6-41.6 13.4-7.8 27.9-12.7 43.2-15.2 8.5-1.4 17-1.9 25.6-2 5.2-.1 8.9 2.1 11.1 6.8 1.8 3.7 3.8 7.2 5.4 11 1.3 3 3.3 4.4 6.6 4.6 12.3.7 24.3 3 35.8 7.3 22.4 8.2 39 23.1 50.1 44.2 7.6 14.4 12.3 29.8 14.9 45.9 1.6 9.6 2.3 19.4 2.9 29.1 1.1 18 2.1 36.1 3.2 54.1.6 10.6 1.4 21.2 2.1 31.8.3 3.9.7 7.8 2.2 11.5 1.2 2.9 2.7 5.6 4.8 8 3.6 3.9 3 9.5.9 13.5-.3.6-1.4 1-2.1 1.3-4.3 1.4-8.7 2.7-13 4.2-9.1 3.4-17.3 8.5-25.1 14.3-.6.4-1.2.8-1.7 1.3-2.5 2.8-5.9 3.6-9.4 4.2-10.7 1.7-21.5 3.5-32.2 5.3-1 .2-2 .4-3.3.7 3 9.6 10.3 15 18.1 19.9-2.9 6.1-5.7 12-8.7 18.2-.4-.2-.9-.4-1.5-.7-5.3-3.1-10.2-6.6-14.8-10.6-2-1.7-2.5-1.8-4.9-.6-2.4 1.3-4.8 2.6-7.1 4-12.3 7.4-25.8 10.7-40 11-12 .2-23.8-1-35.3-4.7-6.8-2.2-13.1-5.4-18.9-9.7-2-1.4-2.6-1.5-4.4.1-10.8 9.7-23.6 16-37 21.4-12.3 5-24.7 9.8-36.9 14.9-9.5 3.9-18.7 8.6-27.5 14.1-7.9 5-14.9 11-20.6 18.5-6.5 8.6-10.3 18.3-12.4 28.8-.4 1.9 0 2.3 1.8 2.5.8.1 1.6 0 2.4 0h180.7c26.3 0 52.6 0 79-.1 2.2 0 3.3.7 4.2 2.6 2.9 5.7 6.7 10.9 10.6 16 .4.5.7 1 1.4 1.9-1.1 0-1.9.1-2.7.1H50.3c-1.7 0-3.4-.1-5-.5-4.9-1.1-7.7-4.5-7.7-9.6 0-6.7 1-13.4 2.6-19.9 5.1-21.5 17-38.5 34.4-51.9 10.4-8 21.8-14.2 33.8-19.4 13.1-5.6 26.3-10.9 39.4-16.4 5.2-2.2 10.4-4.5 15.6-6.8 4.4-1.9 8.5-4.4 12.2-7.5l7.2-6c3.1-2.8 3.8-6.2 4-10.1zm-64.6-36.3c.5.4.7.6 1 .7 4.8 3.1 10.1 5.1 15.6 6.7 11.5 3.4 23.4 5 35.3 6.7 8.4 1.2 16.8 2.2 25.2 3.4 6 .8 9.4 4.7 9.7 10.7.2 3 .2 5.9-.6 8.8-1.1 4.3.4 7.9 4 10.5.9.7 1.9 1.2 2.9 1.7 8.2 4.4 17.2 6.6 26.4 7.4 11.8 1.1 23.5-.1 34.8-4.1 4.2-1.5 8.1-3.5 11.7-6.1 3-2.2 4.4-4.9 3.8-8.7-.4-2.7-.6-5.4-.8-8.1-.5-6.2 2.5-10.3 8.4-11.9.5-.1.9-.2 1.4-.3 2.4-.3 4.8-.6 7.1-1 12.6-2.1 25.3-3.9 37.8-6.3 10.5-2 20.6-5.3 30-10.4.7-.4 1.4-.7 2.1-1.2 1.2-.9 1.3-1.6.6-2.9-.4-.8-1-1.5-1.5-2.2-3.2-4.9-5.5-10.1-6-16-.9-10.8-1.8-21.7-2.4-32.5-.8-15.4-1.3-30.8-2-46.3-.7-15.1-2.2-30.1-5.3-45-2.7-12.8-6.8-25.1-13.4-36.5-9.4-16.3-23-27.4-41-33-10.9-3.4-22.2-4.8-33.6-4.7-5.9 0-10-2.5-12.4-8-1.4-3.3-3.2-6.4-4.9-9.6-.9-1.7-2.3-2.5-4.3-2.4-9.1.5-18.2 1.5-27 3.7-21.1 5.3-38.4 16.3-51.2 34-6.3 8.7-11.1 18.3-15.2 28.3-7.8 19.3-11.8 39.6-14.4 60.2-1.3 10.2-2.4 20.3-3.5 30.5-1.3 11.4-2.5 22.7-3.7 34.1-1 9.3-2.5 18.5-4.7 27.6-1.7 6.9-4 13.5-8.2 19.3-.6.9-1.1 1.8-1.7 2.9z"/><path d="M457.9 348.9l14.2-14.2c-23.9-19.3-65-22.9-95.2 2.3-29.7 24.8-36.2 68.4-14.8 100.9 21.4 32.4 63.8 43.7 98.5 26.2 36.2-18.2 52.7-62.2 34.9-100.9l16.1-16.1c17.4 27 21.9 78.5-13.6 116.3-36 38.4-96 41.6-136.1 7.6-40.7-34.5-45.3-95.3-14.3-135.5 33.7-43.7 97.2-51.6 140.7-16.6.4-.3.8-.6 1.2-1 3.6-3.6 7.1-7.2 10.7-10.7 1-1 1.3-1.8.9-3.2-.5-1.9-.7-3.9-1.1-5.9-.2-.9-.1-1.7.6-2.4 7.3-7.2 14.5-14.5 21.7-21.7.1-.1.3-.1.7-.3 1.3 7 2.6 13.9 3.8 20.9 7.1 1.3 13.9 2.5 21.2 3.9-.7.8-1.2 1.3-1.6 1.8-6.7 6.7-13.4 13.3-20 20.1-1.2 1.2-2.3 1.5-3.8 1.1-1.8-.4-3.6-.6-5.4-1-1.2-.3-2 0-2.7.9-.1.2-.3.3-.5.5-23.9 23.9-47.8 47.7-71.6 71.6-1.1 1.1-1.8 2.2-1.9 3.9-.6 7.4-7.1 13.1-14.7 13.1-7.5 0-14-5.8-14.6-13.2-.7-8.4 4.7-15.2 13.1-16.2 1.6-.2 2.7-.8 3.7-1.8 3.9-4 7.9-7.9 11.9-11.9.6-.6 1.1-1.1 1.8-1.9-1-.5-1.7-.9-2.5-1.2-20.8-8.9-44.4 4.6-47.3 27.1-2.4 18.6 10.3 35.6 29 38.3 19.2 2.8 35.6-10.2 38.6-27.9.5-2.9 1.6-4.9 3.7-6.9 4.9-4.7 9.6-9.6 14.5-14.4.5-.5 1.1-1.1 1.8-1.6 6.3 16.4 3.6 45.1-20.2 62.5-22.5 16.4-53.5 14-73.1-5.7-19.6-19.6-22-50.6-5.8-73 17.3-24.2 51.8-31.3 77.5-13.8z"/></svg>
            </div>
            <div class="a-info">
              <p>Точный таргетинг</p>
              <span>Интересы, пол, возраст, 
              работа, уровень 
              дохода и многое другое. 
              Рекламу увидит только 
              целевая аудитория.</span>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-xl-4">
          <div class="a-elem">
            <div class="a-img">
              <svg style="fill: #E6273C;width: 57px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1761.2 1766"><path d="M865.5 222.7c350.4-.2 635 284.4 635.2 635.1.2 349.7-285.3 635-635.3 634.7-350.3-.3-634.9-285.1-634.5-635.3.4-350.4 284.4-634.4 634.6-634.5zM517.1 857.6c-.3 1.1-.5 1.8-.5 2.4 0 19-1.5 38.1.3 57 3.1 31 13.4 36.2 37.4 35.9h3.5c43.2 0 86.3-.2 129.5.1 25.8.2 49.8 6.7 70.4 22.8 9.8 7.7 19.1 16.1 28.2 24.7 10.8 10.2 22.1 16.2 38 16.2 40.2.1 67.9 33.8 72.3 63 1.7 11.5 1.1 23.6-.2 35.3-4.5 40.1-21.6 74-53.5 99.2-9 7.1-14.8 15-18.8 25.6-17.4 46.6-34.1 93.4-45.9 141.7-3.1 12.7-5.7 25.5-8.7 39 145.8 23.8 280-4.1 404.5-81.5-1.3-3-2.3-5.1-3.2-7.2-12.6-27.7-25.3-55.3-37.8-83-11.2-25-22.1-50.2-33.4-75.1-.9-1.9-3.2-3.9-5.2-4.5-37-10.9-69.7-29-95.6-58-32.3-36.2-44.1-78.6-33.6-126 9.5-42.9 37.8-69.5 78.3-84 25.1-9 51.2-11.6 77.6-11.8 20.1-.2 39.3-4.6 57.7-12.3 23.8-9.9 45-23.7 62-43.3 4.3-4.9 6.9-10.4 6-17.3-.9-7.4-1.1-14.9-2.2-22.3-2.6-17.7-7.9-34.6-16.2-50.6-2.4-4.6-5.1-7-10.6-4.7-3 1.3-6.4 1.7-9.6 2.7-24.3 7.8-42.7 23-57.1 43.8-10.2 14.6-20.6 29.2-34.3 40.8-24.1 20.3-52.3 29.2-83.4 31.6-20.6 1.6-34.6-6.9-44.5-23.9-11.2-19.3-18.7-40-24-61.6-3-12.3-4.9-24.9-1.4-37.5 5.3-19.6 19.7-30.6 37.2-38.4 6.6-2.9 13.5-5.2 20.9-8-5.6-5.7-10.9-11.3-16.3-16.6-7.2-7-10.8-15.6-11.2-25.4-.3-6.3-.2-12.8 1-18.9 3.5-17.3 11.8-31.6 27.6-40.9 15.6-9.2 30.9-19 46.5-28.3 9.3-5.5 17.9-11.4 23.3-21.1 2.3-4.1 4.9-7.9 7.2-12 19.9-35 36.4-71.7 52.5-108.6 6.1-14 12-28.1 18.3-42.9-15-8.4-29.4-17-44.3-24.6-74.4-38-153.3-58.9-236.8-62.3-49.9-2-99.1 2.9-147.7 13.9-2.2.5-4.7 2.4-5.9 4.3-17.5 29.2-30.6 60.5-41.7 92.6-7.4 21.5-14.3 43.3-17.7 65.9-1 6.5-.8 7.3 5.6 9 4.1 1.2 8.4 2.2 12.7 2.5 13.6 1.2 27.3 1.2 40.8 3.2 37.6 5.6 67.5 31.8 67.6 76.1 0 37.2.1 74.3 0 111.5 0 5.6-.4 11.3-1 16.9-5 45.7-39.4 77.5-85.5 79.4-10.6.4-21.3.9-31.9 2-21.5 2.4-33.9 13.7-37.6 35.1-1.8 10.6-2.3 21.5-2.6 32.3-1 28.8-17.1 48.8-45 56.2-24.2 6.4-52.5-6.8-58.6-27.7-.3-.9-1.8-2.1-2.8-2.1-7.3-.4-13.9-.3-20.6-.3zm708.7 443.1c286.2-228.7 278-670.6-3.5-888.7l-1.8 3.6c-11.2 25.5-22 51.3-33.8 76.6-13.8 29.7-28.2 59.2-46.7 86.3-8 11.8-17.8 21.3-30.2 28.5-16.7 9.6-32.9 20.1-49.7 30.4 6.3 6.6 12.5 12.8 18.6 19.2 7.7 8.2 11 17.8 8.5 28.9-2 8.6-4 17.2-6.6 25.6-5.5 17.5-16.9 29.4-34.5 35.2-5.5 1.8-10.8 4.3-16.3 6.2-2.5.9-3.4 2.3-2.7 4.9 2.8 9.6 5.6 19.1 8.4 28.7 1.4 4.9 4.1 6.6 9.1 5 3.9-1.2 8.1-1.6 12.1-2.8 13.4-3.8 22.5-13.1 30.2-24.1 12.9-18.5 25.7-37 43.5-51.3 28.3-22.8 60.7-36.7 95.9-44.8 12.9-3 24.4.1 32.8 10.5 7.4 9.1 14.8 18.5 20.8 28.6 20.8 35.1 29.2 73.5 30.4 114 .5 17-4.2 32-14.8 44.9-7.9 9.6-15.9 19.4-25.3 27.5-45 39.2-97.2 59.8-157.4 59.4-16.9-.1-33.6 2-49.6 7.8-19.4 7.1-33.4 19.3-37.1 40.6-5.3 30.4 3.5 56 27.3 75.9 21.1 17.6 45.4 28.7 72.2 34.7 9.6 2.1 17.1 7.2 21.9 15.8 3.6 6.5 7.4 13 10.5 19.8 18.5 41.6 36.7 83.3 55.1 125 4 9.4 8.4 18.7 12.7 28.1zM298.5 793.6c5.3.6 10.3.9 15.1 1.8 32.9 6 56.1 25.9 74.7 52.4 7.4 10.6 14.3 21.7 22.6 31.6 11.7 13.9 26.7 24.2 42.2 35.4v-65.3c0-31.7 20.8-50.7 47.5-54.3 15.8-2.1 32.2-1.1 48.3-.1 12 .7 23.4 4.4 31.9 14.6.5-4.9.8-9.5 1.3-14 3.3-27.5 13.9-51.2 35.8-69 20.2-16.4 43.8-24 69.3-26.6 9.6-1 19.3-.9 28.9-2.1 14.5-1.7 20.8-8.6 22.2-22.9.3-3.5.5-7 .5-10.5V559.1c0-2.8-.2-5.7-.7-8.4-1.1-7.1-3.1-9.3-10.2-9.9-15.4-1.1-30.9-1.3-46.3-3-15.2-1.6-29.9-6.1-41.9-16-24.1-19.9-31.9-45.9-26-76.1 6.3-31.5 16.3-62 27.8-92 3.2-8.3 6.6-16.5 10.1-25.3-175.2 68.3-328.7 240.5-353.1 465.2zm-4.3 64.4c.7 102.9 26 199 78.1 287.8 50.9 86.8 120.3 155.1 207.6 206.6.2-22 1.5-43.1-5.4-63.6-10.8-32-21.2-64-31.9-96-14-42.1-25.5-84.7-26-129.5-.1-10.1-3.4-18.8-9.2-26.9-8.6-11.9-19.6-21.3-31.5-29.6-22.9-15.8-46.1-31-68.7-47.1-29.2-20.7-54.3-45.5-72-77.1-4.8-8.6-12.5-14.6-20.5-20.2-5.8-4.1-12.6-5.5-20.5-4.4zm412.5 548.9c.6-2.4 1.1-3.8 1.4-5.2 3.3-14.4 6.2-29 9.8-43.3 14.2-55.5 34.1-109 54.5-162.4 3-7.9 8.5-13.7 16-17.8 3.9-2.1 7.8-4.5 11.3-7.2 24.2-19.3 33.8-45.8 34-75.6.1-10.5-3.8-15.9-15.2-15.8-25 .2-47.5-7.6-66.4-24.4-8.4-7.4-17.2-14.3-24.9-22.3-11.8-12.3-26-16.9-42.7-16.7-36.3.4-72.6.1-109 .1-1.7 0-3.5.1-3.9.2 2.8 12.2 5.9 23.8 8 35.7 1.2 7 .5 14.3 1 21.4 2 28.2 8.4 55.6 17.2 82.3 10.8 32.8 22.6 65.3 33.7 98 8.1 23.8 12.8 48.1 12 73.5-.4 15-.1 30-.1 45 0 10.9-.1 11.2 10 15 17.6 6.6 35.2 12.9 53.3 19.5z"/><path d="M1382.2 1405.7c-17.2.1-31.8-14.3-31.9-31.7-.1-17.3 14.3-31.8 31.6-31.9 17.2-.1 31.9 14.4 32 31.7.1 17.1-14.3 31.7-31.7 31.9zm116.3-944.8c17.3 0 31.5 14.2 31.5 31.5 0 17.5-14.3 31.7-31.8 31.7-17.6 0-31.9-14.3-31.7-31.8.1-17.7 14.1-31.5 32-31.4zM532.3 1490.3c-.2 17.6-14.7 31.8-32.1 31.5-17.9-.3-31.4-14.8-31.1-33.4.3-16.4 14.9-30.4 31.8-30.2 17.6.1 31.6 14.5 31.4 32.1zm-151.2-116c-.1 17.2-14.4 31.4-31.6 31.4-17.6 0-32-14.6-31.8-32.2.3-17.5 14.8-31.7 32.1-31.4 17.6.3 31.5 14.6 31.3 32.2zM532.3 225.2c-.2 17.7-14.3 31.8-31.8 31.7-17.9-.1-31.6-14.6-31.4-33.1.2-16.7 14.9-30.7 32-30.4 17.4.2 31.4 14.5 31.2 31.8zM1382 372.9c-17.5-.1-31.7-14.4-31.6-31.9.1-17.4 14.5-31.6 31.9-31.5 17.4.1 31.8 14.6 31.7 31.8-.2 17.5-14.6 31.6-32 31.6zm115.5 881.3c-17.4-.5-31.4-14.8-31.1-32 .3-17.8 14.9-31.8 32.7-31.4 17.1.4 31.2 15.1 30.9 32.2-.4 17.5-15.1 31.7-32.5 31.2zm-235 235.7c.1 17.4-14.4 31.9-31.8 31.8-17.2 0-31.5-14.3-31.6-31.5-.1-17.5 14-31.9 31.6-32 17.3-.2 31.7 14.1 31.8 31.7zm308.7-411.8c-17.5.1-31.8-14-31.9-31.5-.1-17.4 14.2-31.7 31.7-31.8 17.5 0 31.8 14.1 32 31.5.1 17.3-14.3 31.7-31.8 31.8zM233 524c-17.5-.3-31.5-14.4-31.4-31.6.1-18.2 14.6-31.9 33.4-31.6 16.6.3 30.5 15.2 30.1 32.3-.4 17.3-14.9 31.2-32.1 30.9zm1337.4 176.2c-17.4-.4-31.4-14.8-31.1-31.9.2-17.9 14.9-31.8 33.2-31.4 16.7.4 30.8 15.4 30.4 32.2-.5 17.5-15 31.5-32.5 31.1zm-862 862.7c-.1 17.5-14.4 31.8-31.9 31.7-17.4-.1-31.5-14.5-31.4-32.2.1-17.2 14.3-31.4 31.6-31.4 17.4 0 31.8 14.4 31.7 31.9zm378 .1c-.1 17.5-14.5 31.8-31.8 31.6-17.7-.2-31.6-14.6-31.5-32.5.2-17.1 14.6-31.2 31.8-31.1 17.4.2 31.6 14.5 31.5 32zM381.1 341.5c-.2 17.5-14.5 31.4-32.1 31.3-17.4-.1-31.5-14.5-31.3-32.1.1-17.5 14.6-31.6 32-31.3 17.7.4 31.6 14.6 31.4 32.1zm327.3-189.3c0 17.6-14.2 31.7-31.8 31.7-17.5 0-31.6-14.3-31.5-31.9.1-17.3 14.1-31.4 31.5-31.4 17.6-.2 31.8 13.9 31.8 31.6zm-548 548c-17.6 0-31.8-14.1-31.7-31.7 0-17.4 14.1-31.5 31.4-31.6 17.7-.1 31.9 13.9 32 31.5.1 17.5-14 31.7-31.7 31.8zm104.7 522.3c0 17.5-14.2 31.8-31.8 31.8s-31.8-14.3-31.7-31.8c.1-17.2 14.3-31.5 31.5-31.6 17.5-.1 32 14.1 32 31.6zm-105-144.4c-17.6-.2-31.6-14.5-31.4-32.1.2-17.8 14.6-31.5 32.7-31.2 17 .2 30.9 14.9 30.6 32.3-.1 17.3-14.5 31.2-31.9 31zm1102.4-853.2c0 17.6-14.1 31.9-31.6 31.9s-31.7-14.2-31.8-31.8c0-17.2 14.1-31.5 31.4-31.7 17.5-.1 32 14.1 32 31.6zM897.4 1587.3c.1 17.6-13.9 32-31.4 32.1-17.3.1-31.7-14-31.8-31.1-.2-17.9 14-32.3 31.7-32.4 17.1 0 31.4 14.2 31.5 31.4zm189-1434.3c-.4 17.6-14.8 31.2-32.5 30.9-17.5-.3-31.1-15.1-30.7-33.2.4-16.7 15.3-30.6 32.3-30.1 17.7.4 31.3 14.7 30.9 32.4zm508.8 736.1c-17.5-.4-31.5-15-31-32.5.4-17.6 15.1-31.3 33.2-30.8 16.7.4 30.7 15.3 30.3 32.2-.5 17.7-14.9 31.5-32.5 31.1zm-729-793.4c17.7.3 31.5 14.6 31.2 32.2-.3 17.4-14.5 31.2-31.9 31-18.1-.2-31.8-14.6-31.4-33 .4-16.6 15-30.4 32.1-30.2zm-699 762.2c-.2 17.4-14.2 31.3-31.6 31.2-17.6 0-31.8-14.3-31.5-31.9.2-18 14.3-31.6 32.4-31.4 17 .2 30.8 14.6 30.7 32.1z"/></svg>
            </div>
            <div class="a-info">
              <p>Местоположение</p>
              <span>
                  Нужны клиенты со всей 
                  страны или в радиусе 500 метров? Точная ГЕО привязка к месту показа рекламы.
              </span>
            </div>
          </div>
        </div>
      </div>
      <a href="#" class="btn btn-red__bordered js-to-form">
        задать вопрос
      </a>
    </div>
  </section>
  
  <?=$this->render('@frontend/views/blocks/instruments')?>
  
  <section class="section section-build">
    <div class="container">
      <div class="section-title section-title__white">
        <span>как строится</span>
        <p>Работа над проектом</p>
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-4 col-xl-4">
          <div class="step">
            <p><span>#1:</span> Анализ</p>
            <p class="time">1-2 дня</p>
            <div class="descr">
              Анализируем ваш бизнес предлагаем и согласовываем оптимальную стратегию размещения
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4">
          <div class="step">
            <p><span>#2:</span> Структура</p>
            <p class="time">2-3 дня</p>
            <div class="descr">
              Разрабатываем структуру рекламной кампании,
              собираем семантическое ядро
              и готовим рекламные объявления
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4">
          <div class="step">
            <p><span>#3:</span> Наблюдение</p>
            <p class="time">1-2 дня</p>
            <div class="descr">
              Настраиваем аналитику
              для отслеживания эффективности
              рекламной кампании 
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4 offset-lg-2 offset-xl-2">
          <div class="step">
            <p><span>#4:</span> Протекция</p>
            <p class="time">1-3 дня</p>
            <div class="descr">
              Запускаем рекламную кампанию,
              добиваемся прохождения
              модерации всех объявлений
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4">
          <div class="step">
            <p><span>#5:</span> Поддержка</p>
            <p class="time">∞</p>
            <div class="descr">
              Анализируем результаты,
              вносим корректировки, занимаемся
              оптимизацией и управлением
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section section-promotion">
    <div class="container">
      <div class="section-title">
        <span>Ваше продвижение</span>
        <p>В надёжных руках</p>
      </div>
      <div class="row">
        <div class="col-md-8 col-lg-4 col-xl-4">
          <div class="description">
            <p>Мы заслужили доверие клиентов<br/> и высочайшее признание<br/> от профессионального<br/> сообщества.</p>
            <p>Кроме <a href="/otzyv-o-roiter.pdf" target="_blank" >рекомендаций АКАР</a>, являемся официальным<br/> агентством Google и Яндекс.</p>
          </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 text-center">
          <div class="promotion-elem">
            <img src="img/yad.png" alt="">
            <a href="#" class="btn btn-red__bordered">
              ОФИЦИАЛЬНЫЙ ПАРТНЕР
            </a>
          </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 text-center">
          <div class="promotion-elem">
            <img src="img/google.png" alt="">
            <a href="#" class="btn btn-red__bordered">
              ОФИЦИАЛЬНЫЙ ПАРТНЕР
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="section section-cost">
    <div class="container">
      <div class="section-title">
        <span>выберите свой тариф</span>
        <p>Стоимость контекстной рекламы</p>
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-4 col-xl-4">
          <div class="cost-item">
            <p class="tarif"># 1</p>
            <p class="name">Яндекс или Google</p>
            <p class="price">от 35 000 <i class="fas fa-ruble-sign"></i></p>
            <span>со 2-го месяца от 25 000 руб.</span>
            <ul>
              <li><small>10 000 руб.</small> - единоразовый платёж за настройку рекламной кампании</li>
              <li><small>10 % от бюджета</small> - ежемесячный платеж за ведение, но не менее 10 000 руб.</li>
              <li><small>15 000 руб.</small> - минимальный стартовый бюджет на клики</li>
              
            </ul>
            <a href="#" class="btn btn-gray js-to-form">заказать</a>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4">
          <div class="cost-item">
            <p class="tarif"># 2</p>
            <p class="name">Яндекс и Google</p>
            <p class="price">от 60 000 <i class="fas fa-ruble-sign"></i></p>
            <span>со 2-го месяца от 45 000 руб.</span>
            <ul>
              <li> <small>15 000 руб.</small> - единоразовый платёж за настройку рекламных кампаний</li>
              <li> <small>10 % от бюджета</small> - ежемесячный платеж за ведение, но не менее 15 000 руб.</li>
              <li> <small>30 000 руб.</small> - минимальный стартовый бюджет на клики</li>
      
            </ul>
            <a href="#" class="btn btn-gray js-to-form">заказать</a>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-4">
          <div class="cost-item">
            <p class="tarif"># 3</p>
            <p class="name">Аудит</p>
            <p class="price">0 <i class="fas fa-ruble-sign"></i></p>
            <span>уже есть рекламная кампания?</span>
            <ul>
              <li>Бесплатно проверим вашу текущую рекламную кампания</li>
              <li>Честно расскажем какие есть ошибки и как их исправить </li>
              <li>Покажем как снизить расход и повысить эффективность</li>
           
            </ul>
            <a href="#" class="btn btn-gray js-to-form">заказать</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  
	
  <?=$this->render('@frontend/views/blocks/cashier')?>
  <section class="section section-results">
    <div class="container">
      <div class="section-title">
        <span>Уже пробовали Яндекс Директ или Google Ads но</span>
        <p>Не устраивают результаты?</p>
      </div>
      <?php 
        $contactForm = new ContactForm3();
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'sendForm'
            ],
        ]);
      ?>
        <p class="form-title">Оставьте заявку на <span>бесплатный</span> аудит рекламной кампании от специалистов Roiter<br/> и мы расскажем о ошибках сливающих бюджет “в трубу”.</p>
        <div class="row">
          <div class="col-md-12 col-lg-6 col-xl-6">
            <div class="result-item">
              <div class="result-image">
                <svg style="fill: #E6273C;width: 70px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 460.5 422.5"><path d="M286.9 289.8c7.9 1.2 16-1.9 22.9-8.3 7.4-6.9 14.7-14 21.7-21.2 9.8-10.1 23.3-12.7 35.3-6.2 2.8 1.5 5.3 3.6 7.5 5.8 10.9 10.8 21.7 21.6 32.5 32.5 11.6 11.7 12.8 25.6 3.2 38.9-15.1 21.1-30.7 41.6-50.3 58.8-4.2 3.7-8.8 6.9-13.4 10.1-11 7.6-23.1 7.7-35.7 5.6-23.7-3.9-45.4-13.3-66.2-24.8-41-22.8-76.8-52.3-109.7-85.4-28.7-28.8-54.5-59.9-75.7-94.7-13.8-22.6-25.5-46.2-30.8-72.4-1.4-7-2.2-14.2-2.4-21.4-.3-10.8 5.5-19.3 11.7-27.3C48.9 65.1 62.7 53 77.1 41.4c8.2-6.6 16.6-12.9 25.1-19.2 11.8-8.7 26.3-7.9 36.8 2.3 12 11.6 23.8 23.4 35.3 35.4 11.9 12.4 11.1 28.6-1.1 40.8-8.6 8.7-16.8 17.8-25 26.9-5.1 5.6-5.6 12.7-5.4 19.9.6 20 7.5 38.1 17.6 54.9 18.6 31 43.3 55.9 75.4 73.1 15.4 8.3 31.7 14 51.1 14.3zm1 17.4c-23.9-.3-42.2-6.4-59.6-15.7-35.3-18.9-62.9-46-83.2-80.3-9.9-16.7-17-34.7-18.9-54.3-1.3-13.4-1.2-26.7 7.9-37.7 8.4-10 17.5-19.3 26.5-28.7 6.4-6.7 6.7-13.4.2-19.9l-30-30c-10.5-10.5-12.2-10.6-24.2-1.9-18.7 13.8-36.9 28.2-52.5 45.8-9.1 10.2-13.6 21.2-10.8 35.4 3.5 18.3 9.7 35.4 18.1 51.8 16.2 31.7 37.1 60 60.6 86.5 37.3 42.2 78.9 79.3 127.9 107.6 19.2 11.1 39.3 20.2 61.4 24 9.3 1.6 18.8 3.3 27.3-3 3.7-2.8 7.6-5.4 11.1-8.5 19.2-17 34.7-37.2 49.6-57.8 3.2-4.5 2.4-10-1.7-14.1l-34.8-34.8c-6.7-6.7-13.1-6.5-19.7.2-6.8 6.9-13.7 13.8-20.8 20.5-10.7 10-23.2 15.9-34.4 14.9zM234.6 61.6c-1.1-5.3-2.2-10.2-3.4-15.7 43.6-8.3 83 0 116.3 29.1 37.6 32.9 50.9 75.3 44.7 124.9-5.6-.9-10.7-1.7-15.9-2.5 5.6-41.1-4.8-76.9-34.7-105.6-29.9-28.7-65.9-38.1-107-30.2z"/><path d="M330.6 191.6c-4.9-.8-9.9-1.6-15.1-2.4 2.9-20.8-2.7-38.8-17.8-53.2-15.1-14.3-33.4-18.4-53.9-15.2-1.1-5.2-2.2-10.1-3.4-15.6 23.7-4.9 45-.4 63.8 14.6 24.9 19.7 31.4 51.8 26.4 71.8z"/><image width="512" height="512" xlink:href="D:\АБВГ загрузки\hotpng.com (8).png" transform="matrix(.8 0 0 .8 538.322 886.217)" overflow="visible"/></svg>
              </div>
              <div class="result-info">
                <span>Звоните, мы на связи 24/7</span>
                <a href="tel:+74951758756">+7 495 175 87 56 </a>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-lg-6 col-xl-6">
            <div class="result-item">
              <div class="result-image">
              <svg style="fill: #E6273C;width: 80px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 721.6 689.9"><path d="M104.5 585.3V278.2c0-7.4 2.6-12.9 8.8-17.3 25.4-18.3 50.6-36.9 76-55.3 3.1-2.2 4.1-4.5 4.1-8.2-.2-13.5-.1-27-.1-40.5 0-1.6.1-3.2.2-5.4h5.6c19.3 0 38.7.1 58-.1 2.6 0 5.6-1 7.7-2.6 28.9-21.1 57.7-42.4 86.4-63.8 3-2.3 5-2.4 8.1-.1 28.6 21.3 57.2 42.5 86 63.5 2.5 1.8 6 2.9 9.1 2.9 19 .3 38 .1 57 .1h6v22.8c0 8 .2 16-.1 24-.1 3.2 1 5.1 3.6 7 25.6 18.9 51.1 38 76.6 56.9 5.8 4.3 8.6 9.8 8.6 17v306.1c-167.2.1-334 .1-501.6.1zM214.7 173c-.1 1.4-.3 2.8-.3 4.3 0 50.8 0 101.6-.1 152.5 0 3.3 1.4 5.1 3.8 6.9 44.3 32.8 88.6 65.6 132.8 98.6 3.5 2.6 5.6 2.3 8.9-.2 44.1-32.9 88.2-65.6 132.4-98.3 2.8-2.1 4-4.2 4-7.8-.1-50.3-.1-100.6-.1-151 0-1.6-.1-3.2-.2-5H214.7zm-70.5 390.9H566L419.1 417c-20.8 15.7-42.3 31.9-63.8 48-22-16.5-43.5-32.7-65.9-49.7-48.8 50-97.3 99.5-145.2 148.6zm440-15.9V294.7C535.3 331.1 487.1 367 439 402.8L584.2 548zM126.5 294.9v251.8c47.8-47.8 96.1-96 144.4-144.3-47.6-35.5-95.8-71.3-144.4-107.5zm66.5 22.8v-89.3c-20.1 15.1-39.6 29.7-59.5 44.6 19.9 15 39.3 29.6 59.5 44.7zm324.6 0c20.2-15.2 39.6-29.7 59.6-44.7-20-14.6-39.5-28.9-59.6-43.6v88.3zM355.3 108.8c-18.9 14-37.6 27.8-57.1 42.3h114.1c-19.5-14.5-38-28.3-57-42.3z"/><path d="M453.1 211.4v20.9H257.5v-20.9h195.6zm-195.7 79.3v-20.5H453v20.5H257.4zm195.7 58.9H257.5v-20.7h195.6v20.7z"/></svg>
              </div>
              <div class="result-info">
                <span>Напишите, и мы ответим</span>
                  <?=$form->field($contactForm, 'email', [
                      'template' => '{input}{error}
                      <button type="submit" class="form-btn js-icon-button" data-icon="img/r-arr.png">
                        <img src="img/r-arr.png" alt="">
                      </button>',
                  ])
                  ->textInput([
                      'placeholder' => 'Ваш e-mail'
                  ])->label(false)?>
                </div>
            </div>
          </div>
        </div>
        <?=$form->field($contactForm, 'type')
            ->hiddenInput([
                'value' => 'Отправлена форма с сайта Roiter'
            ])->label(false)?>

        <?=$form->field($contactForm, 'BC')
            ->textInput([
                'class' => 'BC',
                'value' => ''
            ])->label(false)?>
      <?php ActiveForm::end();?>
    </div>
  </section>
	<?=$this->render('@frontend/views/blocks/recommends')?>
	<?=$this->render('@frontend/views/blocks/smi')?>
	<?=$this->render('@frontend/views/blocks/form3')?>
</section>