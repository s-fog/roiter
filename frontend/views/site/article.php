<?php

use common\models\Category;

$this->params['seotitle'] = !empty($model->seo_title) ? $model->seo_title : $model->name;
$this->params['seodescription'] = $model->seo_description;
if (!empty($model->og_image)) {
	$this->params['og_image'] = $model->og_image;
}

?>
<section class="content">
	<div class="breadcrumbs">
		<div class="container">
			<ul>
				<li>
					<a href="/">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.84 17"><g data-name="Слой 2"><path fill="#797979" d="M10.92 0L0 8.83h3V17h5.91v-6.61h4.17V17h5.77V8.83h2.99L10.92 0z" data-name="Слой 1"/></g></svg>
						Главная
					</a>
				</li>
				<li>
					<p>></p>
				</li>
				<li>
					<a href="<?=$model->category->url?>"><?=$model->category->name?></a>
				</li>
				<li>
					<p>></p>
				</li>
				<li>
					<span><?=$model->name?></span>
				</li>
			</ul>
		</div>
	</div>
	<section class="article container container-900 content">
		<div class="article__top">
			<div class="article__topLeft">
				<?php 
				$image = '';

				if (!empty($model->human->image)) {
					$filename = explode('.', basename($model->human->image));
					$image = '/images/thumbs/'.$filename[0].'-40-40.'.$filename[1];
				}
				?>
				<div class="articles__itemHuman">
					<div class="articles__itemHumanImage" style="background-image: url(<?=$image?>)"></div>
					<div class="articles__itemHumanName"><?=$model->human->name?></div>
				</div>
				<div class="articles__itemDate"><?=Yii::$app->formatter->asDatetime($model->created_at, 'php:d M Y')?></div>
			</div>
			<div class="article__topRight">
				<div class="articles__itemCategory">#<?=$model->category->name?></div>
			</div>
		</div>
		<div class="article__content">
			<h1 class="article__header"><?= !empty($model->seo_h1) ? $model->seo_h1 : $model->name ?></h1>
			<?php foreach($model->contents as $content) { 
				if (!empty($content->p)) {
					echo $content->p;
				}

				if (!empty($content->image)) {
					echo '<img src="'.$content->image.'" alt="" class="article__image">';
				}

				if (!empty($content->content1_header)) { ?>
					<div class="article__content1">
						<div class="article__content1Header"><?=$content->content1_header?></div>
						<p><?=$content->content1_text?></p>
					</div>
				<?php } 
					if (!empty($content->content2)) { ?>
					<div class="article__content2"><?=$content->content2?></div>
				<?php }
					if (!empty($content->content3)) { 
					$content3_human_image = '';

					if (!empty($content->content3_human_image)) {
						$filename = explode('.', basename($content->content3_human_image));
						$content3_human_image = '/images/thumbs/'.$filename[0].'-40-40.'.$filename[1];
					}
					?>
					<div class="article__content3">
						<?=$content->content3?>
						<div class="article__content3Human">
							<div class="article__content3HumanLeft">
								<div class="article__content3HumanLeftLeft">
									<div class="article__content3HumanImage" style="background-image: url(<?=$content3_human_image?>)"></div>
								</div>
								<div class="article__content3HumanLeftRight">
									<div class="article__content3HumanName"><?=$content->content3_human_name?></div>
									<a href="<?=$content->content3_human_site_link?>" class="article__content3HumanSite"><?=$content->content3_human_site?></a>
								</div>
							</div>
							<div class="article__content3HumanRight">
								<svg viewBox="0 0 55 42" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M17.603 19.0341C16.2484 18.6775 14.8938 18.4966 13.5761 18.4966C11.5413 18.4966 9.84336 18.9218 8.52682 19.4424C9.79606 15.1936 12.8451 7.86244 18.919 7.03684C19.4815 6.96035 19.9425 6.58843 20.0959 6.0878L21.4234 1.74615C21.5354 1.37899 21.469 0.98597 21.24 0.667337C21.0109 0.348703 20.6429 0.136105 20.2304 0.0844061C19.7821 0.028487 19.3252 0 18.8723 0C11.5816 0 4.3614 6.95823 1.31465 16.9213C-0.473819 22.7665 -0.998246 31.5542 3.40717 37.0854C5.87238 40.1805 9.46894 41.8333 14.097 41.9984C14.1161 41.9989 14.1345 41.9995 14.1536 41.9995C19.864 41.9995 24.9277 38.4829 26.4681 33.4486C27.3883 30.439 26.9723 27.2827 25.2958 24.559C23.6371 21.866 20.9054 19.903 17.603 19.0341Z" fill="#D8082E"/>
									<path d="M53.3491 24.5596C51.6904 21.866 48.9587 19.903 45.6563 19.0341C44.3017 18.6775 42.9471 18.4966 41.63 18.4966C39.5951 18.4966 37.8967 18.9218 36.5801 19.4424C37.8494 15.1936 40.8984 7.86244 46.9729 7.03684C47.5354 6.96035 47.9958 6.58843 48.1498 6.0878L49.4773 1.74615C49.5892 1.37899 49.5229 0.985971 49.2938 0.667338C49.0654 0.348705 48.6973 0.136106 48.2842 0.0844074C47.8365 0.0284883 47.3796 1.28857e-06 46.9261 1.28857e-06C39.6355 1.28857e-06 32.4153 6.95824 29.368 16.9213C27.5801 22.7665 27.0556 31.5542 31.4616 37.0865C33.9263 40.181 37.5234 41.8344 42.1509 41.9989C42.17 41.9995 42.1884 42 42.208 42C47.9179 42 52.9821 38.4834 54.5225 33.4491C55.4416 30.4395 55.025 27.2827 53.3491 24.5596Z" fill="#D8082E"/>
								</svg>
							</div>
						</div>
					</div>
				<?php } 
			} ?>
		</div>
		<div class="article__social">
			<script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
			<script src="https://yastatic.net/share2/share.js"></script>
			<div class="article__socialText">Поделиться</div>
			<div class="ya-share2" data-services="vkontakte,facebook,twitter"></div>
		</div>
		<?php 
		$nearArticles = $model->getNearArticles();
		if (!empty($nearArticles)) { ?>
			<div class="article__moreHeader">Вам будет интересно</div>
			<div class="articles">
				<?php foreach($nearArticles as $article) {
					echo $this->render('_article', ['model' => $article]);
				} ?>
			</div>
		<?php } ?>
	</section>
    <?=$this->render('@frontend/views/blocks/form3')?>
</section>