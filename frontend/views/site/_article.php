<?php
$image = '';

if (!empty($model->image)) {
	$filename = explode('.', basename($model->image));
	$image = '/images/thumbs/'.$filename[0].'-250-350.'.$filename[1];
}
?>
<div class="articles__item">
	<div class="articles__itemTop">
		<div class="articles__itemTopLeft">
			<div class="articles__itemTopLeftTop">
				<div class="articles__itemHuman">
					<?php if (!empty($model->human->image)) { 
						$human_image = '';

						if (!empty($model->human->image)) {
							$filename = explode('.', basename($model->human->image));
							$human_image = '/images/thumbs/'.$filename[0].'-40-40.'.$filename[1];
						}
						
						?>
						<div class="articles__itemHumanImage" style="background-image: url(<?=$human_image?>)"></div>
					<?php } ?>
					<div class="articles__itemHumanName"><?=$model->human->name?></div>
				</div>
				<div class="articles__itemDate"><?=Yii::$app->formatter->asDatetime($model->created_at, 'php:d M Y')?></div>
				<div class="articles__itemCategory">#<?=$model->category->name?></div>
			</div>
			<div class="articles__itemTopLeftBottom">
				<a href="<?=$model->url?>" class="articles__itemHeader"><?=$model->name?></a>
			</div>
		</div>
		<div class="articles__itemTopRight">
			<?=$model->icon?>
		</div>
	</div>
	<?php if (!empty($model->text)) { ?>
		<div class="articles__itemText"><?=$model->text?></div>
	<?php } ?>
	<?php if (!empty($model->image)) { ?>
		<div class="articles__itemImage"><img src="<?=$model->image?>" alt=""></div>
	<?php } ?>
	<div class="articles__itemFeatures">
		<?php if (!empty($model->feature1_red) && !empty($model->feature1_text)) { ?>
			<div class="articles__itemFeature">
				<div><?=$model->feature1_red?></div>
				<span><?=$model->feature1_text?></span>
			</div>
		<?php } ?>
		<?php if (!empty($model->feature2_red) && !empty($model->feature2_text)) { ?>
			<div class="articles__itemFeature">
				<div><?=$model->feature2_red?></div>
				<span><?=$model->feature2_text?></span>
			</div>
		<?php } ?>
		<?php if (!empty($model->feature3_red) && !empty($model->feature3_text)) { ?>
			<div class="articles__itemFeature">
				<div><?=$model->feature3_red?></div>
				<span><?=$model->feature3_text?></span>
			</div>
		<?php } ?>
	</div>
	<div class="articles__itemBottom">
		<div class="articles__itemBottomLeft">
			<div class="articles__itemCount">
				<svg viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M17.8856 8.64991C17.7248 8.42993 13.8934 3.26375 8.99991 3.26375C4.10647 3.26375 0.274852 8.42993 0.114223 8.6497C-0.0380743 8.85838 -0.0380743 9.14143 0.114223 9.35012C0.274852 9.57009 4.10647 14.7363 8.99991 14.7363C13.8934 14.7363 17.7248 9.57005 17.8856 9.35029C18.0381 9.14164 18.0381 8.85838 17.8856 8.64991ZM8.99991 13.5495C5.39537 13.5495 2.27345 10.1206 1.3493 8.99961C2.27226 7.87766 5.38764 4.45056 8.99991 4.45056C12.6043 4.45056 15.726 7.87886 16.6505 9.00042C15.7276 10.1223 12.6122 13.5495 8.99991 13.5495Z" fill="#939393"/>
					<path d="M8.99991 5.43956C7.03671 5.43956 5.43945 7.03682 5.43945 9.00002C5.43945 10.9632 7.03671 12.5605 8.99991 12.5605C10.9631 12.5605 12.5604 10.9632 12.5604 9.00002C12.5604 7.03682 10.9631 5.43956 8.99991 5.43956ZM8.99991 11.3736C7.69104 11.3736 6.62629 10.3089 6.62629 9.00002C6.62629 7.69118 7.69107 6.6264 8.99991 6.6264C10.3087 6.6264 11.3735 7.69118 11.3735 9.00002C11.3735 10.3089 10.3088 11.3736 8.99991 11.3736Z" fill="#939393"/>
				</svg>
				<span><?=$model->visit_count?></span>
			</div>
		</div>
		<div class="articles__itemBottomRight">
			<a href="<?=$model->url?>" class="articles__itemMore">
				<span>Подробнее</span>
				<svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M5.7628 7L5.47552e-05 1.23725L1.2373 3.0598e-07L8.2373 7L1.2373 14L5.5259e-05 12.7628L5.7628 7Z" fill="#D8082E"/>
				</svg>
			</a>
		</div>
	</div>
</div>