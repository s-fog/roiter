<?php

use frontend\models\ContactForm3;
use yii\bootstrap\ActiveForm;

$this->params['seotitle'] = 'Документы | Roiter';
$this->params['seodescription'] = 'В данном разделе собрали основные документы, которые могут потребоваться в процессе сотрудничества, вы можете ознакомиться с ними или скачать. ';
?>
<section class="content">
  <div class="breadcrumbs">
    <div class="container">
      <ul>
        <li>
          <a href="/">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.84 17"><g data-name="Слой 2"><path fill="#797979" d="M10.92 0L0 8.83h3V17h5.91v-6.61h4.17V17h5.77V8.83h2.99L10.92 0z" data-name="Слой 1"/></g></svg>
            Главная
          </a>
        </li>
        <li>
          <p>></p>
        </li>
        <li>
          <span>Документы</span>
        </li>
      </ul>
    </div>
  </div>
  <section class="section-headers">
    <div class="container">
      <span>узнайте подробнее про</span>
      <p>Документы ROITER</p>
    </div>
  </section>
  <section class="section section-price" style="padding: 40px 0 50px;">
      <div class="container">
        <div class="price-elem">
          <div class="row flex">
            <div class="col-sm-12 col-md-7 col-lg-6 col-xl-6 flex-middle info">
              <div class="section-title">
                <span>тут можно скачать</span>
                <p>Документы в удобном <br> формате</p>
              </div>
              <ul class="links">
                <li><a href="/dogovor-context.pdf" target="_blank">Договор "Контекстная реклама"</a></li>
                <li><a href="/rekvizity.pdf" target="_blank">Карточка организации</a></li>
                <li><a href="/prezentacia-agentstva-roiter.pdf" target="_blank">Презентация агентства</a></li>
                <li><a href="/politika-pd.pdf" target="_blank">Политика обработки персональных данных</a></li>
              </ul>
            </div>
            <div class="col-sm-12 col-md-5 col-lg-6 col-xl-6 flex-middle">
              <img src="/img/folder.png" alt="">
            </div>
          </div>
        </div>
      </div>
  </section>
	<?=$this->render('@frontend/views/blocks/form3')?>
</section>