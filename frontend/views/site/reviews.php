<?php

use common\models\Review;

$this->params['seotitle'] = 'Отзывы | ROITER';
$this->params['seodescription'] = 'Отзывы от клиентов о сотрудничестве с агентством Ройтер';

$reviews = Review::find()->orderBy('sort_order', SORT_DESC)->all();
?>
<section class="content">
	<div class="breadcrumbs">
		<div class="container">
			<ul>
				<li>
					<a href="/">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.84 17"><g data-name="Слой 2"><path fill="#797979" d="M10.92 0L0 8.83h3V17h5.91v-6.61h4.17V17h5.77V8.83h2.99L10.92 0z" data-name="Слой 1"/></g></svg>
						Главная
					</a>
				</li>
				<li>
					<p>></p>
				</li>
				<li>
					<span>Отзывы</span>
				</li>
			</ul>
		</div>
	</div>
	<section class="section-headers">
		<div class="container">
			<span>узнайте подробнее про</span>
			<p>Отзывы</p>
		</div>
	</section>
	<section class="reviews container">
		<?php foreach($reviews as $review) {
			echo $this->render('_review', ['review' => $review]);	
		} ?>
	</section>
    <?=$this->render('@frontend/views/blocks/form3')?>
</section>