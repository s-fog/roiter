<?php
$this->params['seotitle'] = 'Продукты и сервисы | Roiter';
$this->params['seodescription'] = 'Ознакомьтесь с сервисами от Roiter, уверены вы найдете для себя что-нибудь интересное и полезное!';
?>
<section class="content">
  <div class="breadcrumbs">
    <div class="container">
      <ul>
        <li>
          <a href="/">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.84 17"><g data-name="Слой 2"><path fill="#797979" d="M10.92 0L0 8.83h3V17h5.91v-6.61h4.17V17h5.77V8.83h2.99L10.92 0z" data-name="Слой 1"/></g></svg>
            Главная
          </a>
        </li>
        <li>
          <p>></p>
        </li>
        <li>
          <span>Продукты</span>
        </li>
      </ul>
    </div>
  </div>
  <section class="section-headers">
    <div class="container">
      <span>узнайте подробнее про</span>
      <p>Продукты ROITER</p>
    </div>
  </section>
  <?=$this->render('@frontend/views/blocks/products')?>
  <?=$this->render('@frontend/views/blocks/form3')?>
</section>