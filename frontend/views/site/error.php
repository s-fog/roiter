<?php
$this->context->layout = 'error';
$this->params['seotitle'] = '404';
$this->params['seodescription'] = '404';
?>

    <div class="topText">
        <div class="container">
            <div class="topText__inner">
                <h1 class="topText__header">УПС, ЧТО-ТО ПОШЛО НЕ ТАК...<br>ТАКОЙ СТРАНИЦЫ НЕ СУЩЕСТВУЕТ</h1>
                <div class="topText__text">
                    <a href="/" class="link">Перейти на главную страницу</a>
                </div>
            </div>
        </div>
    </div>