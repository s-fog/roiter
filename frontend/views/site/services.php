<?php
$this->params['seotitle'] = 'Услуги | ROITER';
$this->params['seodescription'] = 'Ознакомьтесь подробнее с услугами нашей компании и начните продвижение своего бизнеса вместе с нами!';
?>
<section class="content">
	<div class="breadcrumbs">
		<div class="container">
			<ul>
				<li>
					<a href="/">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.84 17"><g data-name="Слой 2"><path fill="#797979" d="M10.92 0L0 8.83h3V17h5.91v-6.61h4.17V17h5.77V8.83h2.99L10.92 0z" data-name="Слой 1"/></g></svg>
						Главная
					</a>
				</li>
				<li>
					<p>></p>
				</li>
				<li>
					<span>Услуги</span>
				</li>
			</ul>
		</div>
	</div>
	<section class="section-headers">
		<div class="container">
			<span>узнайте подробнее про</span>
			<p>Услуги компании</p>
		</div>
	</section>
	
	<?=$this->render('@frontend/views/blocks/services')?>
	<?=$this->render('@frontend/views/blocks/form3')?>
</section>