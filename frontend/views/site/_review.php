<?php

use common\models\Review;

$image = '';

if (!empty($review->image)) {
	$filename = explode('.', basename($review->image));
	$image = '/images/thumbs/'.$filename[0].'-250-350.'.$filename[1];
}

$man_image = '';

if (!empty($review->man_image)) {
	$filename = explode('.', basename($review->man_image));
	$man_image = '/images/thumbs/'.$filename[0].'-40-40.'.$filename[1];
}
?>
<div class="reviews__item">
    
	<div class="reviews__itemLeft" data-fancybox data-src="<?=$review->image?>">
		<img src="<?=$image?>" alt="">
		<div class="reviews__itemLoop">
			<svg width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg">
				<path d="M12.8393 12.066L9.88945 9.11613C11.6891 6.92658 11.5666 3.67626 9.52165 1.63133C7.34621 -0.544112 3.80653 -0.544112 1.63109 1.63133C-0.544349 3.80677 -0.544349 7.34645 1.63109 9.52189C3.67548 11.5663 6.92539 11.6901 9.1159 9.88969L12.0658 12.8395C12.2794 13.0532 12.6257 13.0532 12.8393 12.8395C13.0529 12.6259 13.0529 12.2796 12.8393 12.066ZM8.74812 8.74833C6.99922 10.4972 4.15355 10.4972 2.40465 8.74833C0.655741 6.99943 0.655741 4.15376 2.40465 2.40486C4.1535 0.656032 6.99916 0.655896 8.74812 2.40486C10.497 4.15376 10.497 6.99943 8.74812 8.74833Z" fill="#868686"/>
				<path d="M8.25486 5.02985H6.12369V2.89868C6.12369 2.59659 5.8788 2.3517 5.5767 2.3517C5.27461 2.3517 5.02972 2.59659 5.02972 2.89868V5.02985H2.89855C2.59645 5.02985 2.35156 5.27474 2.35156 5.57684C2.35156 5.87893 2.59645 6.12382 2.89855 6.12382H5.02972V8.25499C5.02972 8.55709 5.27461 8.80198 5.5767 8.80198C5.8788 8.80198 6.12369 8.55709 6.12369 8.25499V6.12382H8.25486C8.55695 6.12382 8.80184 5.87893 8.80184 5.57684C8.80184 5.27474 8.55695 5.02985 8.25486 5.02985Z" fill="#868686"/>
			</svg>
			<span>Увеличить</span>
		</div>
	</div>
	<div class="reviews__itemRight">
		<div class="reviews__itemRightTop">
			<div class="reviews__itemRightTopLeft">
				<?php if (!empty($review->man_name)) { ?>
					<div class="reviews__man">
						<?php if (!empty($man_image)) { ?>
						<div class="reviews__manImage" style="background-image: url(<?=$man_image?>)"></div>
						<?php } ?>
						<div class="reviews__manName"><?=$review->man_name?><?=!empty($review->man_function) ? ', ' : ''?></div>
						<div class="reviews__manFunction"><?=$review->man_function?></div>
					</div>
				<?php } ?>
			</div>
			<div class="reviews__itemRightTopRight">
				<?=$review->icon?>
			</div>
		</div>
		<div class="reviews__name"><?=$review->header?></div>
		<div class="reviews__text"><?=$review->text?></div>
		<?php if (!empty($review->link)) { ?>
			<a href="<?=$review->link?>" target="_blank" rel="nofollow" class="btn btn-red">
				<?=empty($review->button_text) ? 'перейти на сайт' : $review->button_text?>
			</a>
		<?php } ?>
	</div>
</div>