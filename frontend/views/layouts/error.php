<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\models\Textpage;
use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <?=$this->render('_head')?>
<body>
<?php $this->beginBody() ?>
    <?=$this->render('_header')?>
    <section class="content">
        <div class="breadcrumbs">
            <div class="container">
                <ul>
                    <li>
                        <a href="/">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.84 17"><g data-name="Слой 2"><path fill="#797979" d="M10.92 0L0 8.83h3V17h5.91v-6.61h4.17V17h5.77V8.83h2.99L10.92 0z" data-name="Слой 1"/></g></svg>
                            Главная
                        </a>
                    </li>
                    <li>
                        <p>></p>
                    </li>
                    <li>
                        <span>404 ошибка</span>
                    </li>
                </ul>
            </div>
        </div>
        <section class="section section-error">
            <div class="container">
                <div class="error-content">
                    <span>упс! похоже у нас</span>
                    <p class="error-text">404</p>
                    <div class="redirect">
                        <p>ошибка: страница не найдена.</p>
                        <p>Давайте вернёмся на <a href="/">главную страницу</a></p>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <?=$this->render('_footer')?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>