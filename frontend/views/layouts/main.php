<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\models\Textpage;
use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <?=$this->render('_head')?>
<body>
<?php $this->beginBody() ?>
    <?=$this->render('_header')?>
    <?=$content?>
    <?=$this->render('_footer')?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
