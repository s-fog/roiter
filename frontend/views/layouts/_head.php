<?php
use yii\helpers\Html;
?>
<head>
    <meta charset="<?=Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$this->params['seotitle']?></title>
    <meta name="description" content="<?=$this->params['seodescription']?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.png">
    <?php if (isset($this->params['og_image'])) { ?>
        <meta property="og:image" content="https://www.roiter.ru<?=$this->params['og_image']?>">
    <?php } ?>
    <?= Html::csrfMetaTags() ?>
    <?=$this->render('_metrics')?>
    <?php $this->head() ?>
</head>