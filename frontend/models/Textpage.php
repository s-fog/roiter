<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * ContactForm is the model behind the contact form.
 */
class Textpage extends Model
{
    public static $SocialAlias  = 'social';
    public static $ContextAlias  = 'context';
    public static $MediaAlias  = 'media';
    public static $ContactsAlias  = 'contacts';
    public static $SeoAlias  = 'seo';
    public static $DevAlias  = 'dev';
    public $id;

    public static function getUrl($id) {
        if ($id == 0) {
            return Url::to(['site/view', 'alias' => Textpage::$SocialAlias]);
        } else if ($id == 1) {
            return Url::to(['site/view', 'alias' => Textpage::$ContextAlias]);
        } else if ($id == 2) {
            return Url::to(['site/view', 'alias' => Textpage::$MediaAlias]);
        } else if ($id == 3) {
            return Url::to(['site/view', 'alias' => Textpage::$ContactsAlias]);
        } else if ($id == 4) {
            return Url::to(['site/view', 'alias' => Textpage::$SeoAlias]);
        } else if ($id == 5) {
            return Url::to(['site/view', 'alias' => Textpage::$DevAlias]);
        }
    }
}
