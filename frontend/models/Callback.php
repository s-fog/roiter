<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class Callback extends Forms
{
    public function rules()
    {
        return [
            [['phone'], 'required'],
            ['phone', 'string'],
        ];
    }
}
