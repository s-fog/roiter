<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class Forms extends Model
{
    public $name;
    public $site;
    public $phone;
    public $email;
    public $message;
    public $BC;
    public $type;
    public $url;

    public function send($post, $files, $sendToUser = false) {
        $labels = array(
            'name' => 'Имя',
            'email' => 'Эл. адрес',
            'site' => 'Сайт',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
        );

        $type = $post['type'];
        $msg = '';
        $to = 'hello@roiter.ru';
        $headers = "Content-type: text/html; charset=\"utf-8\"\n";
        $headers .= "From: <roiteradv@yandex.ru>\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Date: ". date('D, d M Y h:i:s O') ."\n";
        unset($post['type']);

        if (isset($_COOKIE['utm_source']) && !empty($_COOKIE['utm_source'])) {
            foreach($_COOKIE as $key => $value) {
                if (strstr($key, 'utm')) {
                    $post[$key] = $value;
                }
            }
        }

        foreach($post as $name=>$value){
            $label = array_key_exists($name, $labels) ? $labels[$name] : $name;
            $value = htmlspecialchars($value);
            if(strlen($value)) {
                if ($name == 'url') {
                    $msg .= "<p><b>$label</b>: <a href='$value'>$value</a></p>";
                } else {
                    $msg .= "<p><b>$label</b>: ".urldecode($value)."</p>";
                }
            }
        }

        $body = $msg;
        if (!empty($files)) {
            $msg .= 'Плюс к этому письму приложен файл';
            $boundary = "--" . md5(uniqid(time()));
            $headers = "MIME-Version: 1.0;\n";
            $headers .= "From: <roiteradv@yandex.ru>\n";
            $headers .= "Content-Type: multipart/mixed; boundary=\"$boundary\"";
            $body = "--$boundary\n";
            $body .= "Content-Type: text/html; charset=UTF-8\n";
            $body .= "Content-Transfer-Encoding: base64\n";
            $body .= "\n";
            $body .= chunk_split(base64_encode($msg));

            $i = 0;
            while($i < count($files['name']['file'])) {
                $fp = fopen($files["tmp_name"]['file'][$i], "rb");

                if (!$fp) {
                    echo "Cannot open file";
                    exit();
                }

                $data = fread($fp, filesize($files["tmp_name"]['file'][$i]));
                fclose($fp);
                $name = $files["name"]['file'][$i];

                $body .= "\n\n--$boundary\n";
                $body .= "Content-Type: " . $files["type"]['file'][$i] . "; name=\"$name\"\n";
                $body .= "Content-Transfer-Encoding: base64 \n";
                $body .= "Content-Disposition: attachment; filename=\"$name\"\n";
                $body .= "\n";
                $body .= chunk_split(base64_encode($data));

                $i++;
            }

            $body .= "\n--$boundary--\n";
        }

        $emailSendError = false;
        foreach(explode(',', $to) as $email) {
            if(!mail($email, $type, $body, $headers)) {
                $emailSendError = true;
            }
        }

        if ($sendToUser) {
            $boundary = "--" . md5(uniqid(time()));
            $headers = "MIME-Version: 1.0;\n";
            $headers .= "From: <roiteradv@yandex.ru>\n";
            $headers .= "Content-Type: multipart/mixed; boundary=\"$boundary\"";

            $html = '
                <p>Здравствуйте!</p>
                <p>Коммерческое предложение по рекламе с оплатой за привлечение клиентов во вложении. Также, во вложении вы найдете нашу презентацию. <br>Если есть дополнительные вопросы - пожалуйста обращайтесь!</p>
                <hr>
                <p>
                С уважением, ROITER
                <br>
                e-mail: <a href="mailto:hello@roiter.ru">hello@roiter.ru</a>
                <br>
                тел.: +7 495 185 09 88
                <br>
                сайт: <a href="https://www.roiter.ru/">https://www.roiter.ru/</a>
                </p>
            ';

            $userBody = "--$boundary\n";
            $userBody .= "Content-Type: text/html; charset=UTF-8\n";
            $userBody .= "Content-Transfer-Encoding: base64\n";
            $userBody .= "\n";
            $userBody .= chunk_split(base64_encode($html));

            $fileName = 'promo-roiter.pdf';
            if (strstr($_SERVER['HTTP_HOST'], 'roiter.ru')) {
                $filePath = $_SERVER['DOCUMENT_ROOT'].'/www/promo-roiter.pdf';
            } else {
                $filePath = $_SERVER['DOCUMENT_ROOT'].'/promo-roiter.pdf';
            }
            $fp = fopen($filePath, "rb");
            if (!$fp) {
                echo "Cannot open file";
                exit();
            }
            $fileData = fread($fp, filesize($filePath));

            $userBody .= "\n\n--$boundary\n";
            $userBody .= "Content-Type: application/pdf; name=\"$fileName\"\n";
            $userBody .= "Content-Transfer-Encoding: base64 \n";
            $userBody .= "Content-Disposition: attachment; filename=\"$fileName\"\n";
            $userBody .= "\n";
            $userBody .= chunk_split(base64_encode($fileData));

            $fileName = 'prezentacia-agentstva-roiter.pdf';
            if (strstr($_SERVER['HTTP_HOST'], 'roiter.ru')) {
                $filePath = $_SERVER['DOCUMENT_ROOT'].'/www/prezentacia-agentstva-roiter.pdf';
            } else {
                $filePath = $_SERVER['DOCUMENT_ROOT'].'/prezentacia-agentstva-roiter.pdf';
            }
            $fp = fopen($filePath, "rb");
            if (!$fp) {
                echo "Cannot open file";
                exit();
            }
            $fileData = fread($fp, filesize($filePath));

            $userBody .= "\n\n--$boundary\n";
            $userBody .= "Content-Type: application/pdf; name=\"$fileName\"\n";
            $userBody .= "Content-Transfer-Encoding: base64 \n";
            $userBody .= "Content-Disposition: attachment; filename=\"$fileName\"\n";
            $userBody .= "\n";
            $userBody .= chunk_split(base64_encode($fileData));

            $userBody .= "\n--$boundary--\n";
            mail($post['email'], 'Коммерческое предложение по интернет-рекламе от Roiter', $userBody, $headers);
        }

        if ($emailSendError) {
            echo 'error';
        } else {
            echo 'success';
        }
    }

    public function attributeLabels() {

        return  [
            'email' => 'Email',
        ];
    }
}
