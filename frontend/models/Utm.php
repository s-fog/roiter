<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class Utm extends Model
{
    public static function save()
    {
        if (isset($_GET['utm_source']) && !empty($_GET['utm_source'])) {
            foreach($_GET as $key => $value) {
                if (strstr($key, 'utm')) {
                    setcookie($key, $value, time()+3600);
                }
            }

            return true;
        } else {
            return false;
        }
    }
}
