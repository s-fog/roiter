<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class ContactForm2 extends Forms
{
    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'email'],
        ];
    }
}
