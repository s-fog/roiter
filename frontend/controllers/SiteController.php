<?php
namespace frontend\controllers;

use common\models\Article;
use common\models\Category;
use frontend\models\Utm;
use Yii;
use frontend\models\SignupForm;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actionIndex()
    {
        if (Utm::save()) {
            return $this->redirect(['site/index']);
        }

        return $this->render('index');
    }

    public function actionView($alias, $alias2 = '')
    {
        if (Utm::save()) {
            return $this->redirect(['site/view', 'alias' => $alias]);
        }

        if (!empty($alias2)) {
            $article = Article::find()->where(['slug' => $alias2])->one();

            if ($article) {
                $article->visit_count++;
                $article->save();

                return $this->render('article', ['model' => $article]);
            }

            throw new \yii\web\NotFoundHttpException();
        }

        //////////////////////////////////////////////////////////////////////////////
        
        $category = Category::find()->where(['slug' => $alias])->one();

        if ($category) {
            list($articles, $pages) = Article::getItems($category->id);
            
            return $this->render('blog', [
                'model' => $category,
                'articles' => $articles,
                'pages' => $pages,
            ]);
        }

        ///////////////////////////////////////////////////////////////////////////////
        
        $documentRoot = str_replace('/www', '', $_SERVER['DOCUMENT_ROOT']);
        $parentRoot = $documentRoot . '/' . Yii::getAlias('frontend') . '/views/site/';
        $file = $alias.'.php';

        if (is_file($parentRoot.$file)) {
            return $this->render($file);
        } else if ($alias == 'sitemap.xml') {
            return $this->renderPartial('@frontend/views/sitemap/index');
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
    }

    public function actionError()
    {
        return $this->render('error');
    }

    /*public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }*/
}
