<?php

namespace frontend\controllers;

use frontend\models\Callback;
use frontend\models\ContactForm;
use frontend\models\ContactForm2;
use frontend\models\ContactForm3;
use frontend\models\ContactFormLanding;
use Yii;

class MailController extends \yii\web\Controller
{
    public function actionIndex() {
        if (isset($_POST['ContactForm'])) {
            if (!empty($_POST['ContactForm']) && isset($_POST['ContactForm']['type']) && !strlen($_POST['ContactForm']['BC'])) {
                $form = new ContactForm();
                $post = $_POST['ContactForm'];
                $form->send($post, $_FILES);
            }
        } else if (isset($_POST['ContactForm2'])) {
            if (!empty($_POST['ContactForm2']) && isset($_POST['ContactForm2']['type']) && !strlen($_POST['ContactForm2']['BC'])) {
                $form = new ContactForm2();
                $post = $_POST['ContactForm2'];
                $form->send($post, $_FILES);
            }
        } else if (isset($_POST['ContactForm3'])) {
            if (!empty($_POST['ContactForm3']) && isset($_POST['ContactForm3']['type']) && !strlen($_POST['ContactForm3']['BC'])) {
                $form = new ContactForm3();
                $post = $_POST['ContactForm3'];
                $form->send($post, $_FILES);
            }
        } else if (isset($_POST['ContactFormLanding'])) {
            if (!empty($_POST['ContactFormLanding']) && isset($_POST['ContactFormLanding']['type']) && !strlen($_POST['ContactFormLanding']['BC'])) {
                $form = new ContactFormLanding();
                $post = $_POST['ContactFormLanding'];
                $form->send($post, $_FILES, true);
            }
        } else if (isset($_POST['Callback'])) {
            if (!empty($_POST['Callback']) && isset($_POST['Callback']['type']) && !strlen($_POST['Callback']['BC'])) {
                $form = new Callback();
                $post = $_POST['Callback'];
                $form->send($post, $_FILES, true);
            }
        }
    }
}
