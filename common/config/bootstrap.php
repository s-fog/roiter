<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@www', dirname(dirname(__DIR__)) . '/www');
Yii::setAlias('@uploadPath', dirname(dirname(__DIR__)) . '/www/uploads');
Yii::setAlias('@thumbsPath', dirname(dirname(__DIR__)) . '/www/images/thumbs');
