<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property string|null $header
 * @property string|null $image
 * @property string|null $icon
 * @property string|null $man_image
 * @property string|null $man_name
 * @property string|null $man_function
 * @property string|null $text
 * @property string|null $link
 * @property string|null $button_text
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }
    
    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'sort_order'
            ],
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['header', 'text'], 'required'],
            [['image', 'man_image'], 'required', 'on' => 'create'],
            [['icon', 'text', 'link'], 'string'],
            [['header', 'man_name', 'man_function', 'button_text'], 'string', 'max' => 255],
            [['sort_order', 'created_at', 'updated_at'], 'integer'],
            [['image'], 'image', 'maxSize' => 1000000,  'minWidth' => 250, 'minHeight' => 350, 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['man_image'], 'image', 'maxSize' => 1000000,  'minWidth' => 40, 'minHeight' => 40, 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'header' => 'Заголовок',
            'image' => 'Изображение(250х350)',
            'icon' => 'Иконка',
            'man_image' => 'Изображение человека(40x40)',
            'man_name' => 'Имя человека',
            'man_function' => 'Должность человека',
            'text' => 'Текст',
            'link' => 'Ссылка',
            'button_text' => 'Текст кнопки',
        ];
    }
}
