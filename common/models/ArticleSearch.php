<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Article;

/**
 * ArticleSearch represents the model behind the search form of `common\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'human_id', 'category_id', 'visit_count'], 'integer'],
            [['name', 'slug', 'seo_title', 'seo_h1', 'seo_description', 'icon', 'text', 'image', 'feature1_red', 'feature1_text', 'feature2_red', 'feature2_text', 'feature3_red', 'feature3_text'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'human_id' => $this->human_id,
            'category_id' => $this->category_id,
            'visit_count' => $this->visit_count,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_h1', $this->seo_h1])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'feature1_red', $this->feature1_red])
            ->andFilterWhere(['like', 'feature1_text', $this->feature1_text])
            ->andFilterWhere(['like', 'feature2_red', $this->feature2_red])
            ->andFilterWhere(['like', 'feature2_text', $this->feature2_text])
            ->andFilterWhere(['like', 'feature3_red', $this->feature3_red])
            ->andFilterWhere(['like', 'feature3_text', $this->feature3_text]);

        return $dataProvider;
    }
}
