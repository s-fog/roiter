<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "content".
 *
 * @property int $id
 * @property int|null $article_id
 * @property string|null $p
 * @property string|null $image
 * @property string|null $content1_header
 * @property string|null $content1_text
 * @property string|null $content2
 * @property string|null $content3
 * @property string|null $content3_human_image
 * @property string|null $content3_human_name
 * @property string|null $content3_human_site
 * @property string|null $content3_human_site_link
 * @property int|null $sort_order
 */
class Content extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['article_id', 'sort_order'], 'integer'],
            [['p', 'content1_text', 'content2', 'content3', 'content3_human_site_link'], 'string'],
            [['content1_header', 'content3_human_name', 'content3_human_site'], 'string', 'max' => 255],
            [['image'], 'image', 'maxSize' => 1500000,  'minWidth' => 870, 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['content3_human_image'], 'image', 'maxSize' => 1000000,  'minWidth' => 40, 'minHeight' => 40, 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'p' => 'Просто html',
            'image' => 'Изображение(870x)',
            'content1_header' => 'Заголовок',
            'content1_text' => 'Текст',
            'content2' => 'Контент',
            'content3' => 'Контент',
            'content3_human_image' => 'Изображение человека(40х40)',
            'content3_human_name' => 'Имя человека',
            'content3_human_site' => 'Текст ссылки',
            'content3_human_site_link' => 'Ссылка',
            'sort_order' => 'Sort Order',
        ];
    }
}
