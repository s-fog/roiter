<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "articles".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $seo_title
 * @property string|null $seo_h1
 * @property string|null $seo_description
 * @property int|null $human_id
 * @property int|null $category_id
 * @property string|null $icon
 * @property string|null $text
 * @property string|null $image
 * @property string|null $feature1_red
 * @property string|null $feature1_text
 * @property string|null $feature2_red
 * @property string|null $feature2_text
 * @property string|null $feature3_red
 * @property string|null $feature3_text
 * @property string|null $og_image
 * @property int|null $visit_count
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articles';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => \nsept\behaviors\CyrillicSlugBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['human_id', 'category_id', 'name'], 'required'],
            [['created_at', 'updated_at', 'human_id', 'category_id', 'visit_count'], 'integer'],
            [['seo_description', 'icon', 'text'], 'string'],
            [['visit_count'], 'default', 'value' => 0],
            [['name', 'slug', 'seo_title', 'seo_h1', 'feature1_red', 'feature1_text', 'feature2_red', 'feature2_text', 'feature3_red', 'feature3_text'], 'string', 'max' => 255],
            [['image'], 'image', 'maxSize' => 1500000,  'minWidth' => 820, 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['og_image'], 'image', 'maxSize' => 1500000,  'minWidth' => 1200, 'minHeight' => 627, 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Урл',
            'seo_title' => 'Seo Title',
            'seo_h1' => 'Seo H1',
            'seo_description' => 'Seo Description',
            'human_id' => 'Публикующий',
            'category_id' => 'Категория',
            'icon' => 'Иконка',
            'text' => 'Текст',
            'image' => 'Изображение(820х)',
            'og_image' => 'Изображение для соц сетей(1200x627)',
            'feature1_red' => 'Feature1 Red',
            'feature1_text' => 'Feature1 Text',
            'feature2_red' => 'Feature2 Red',
            'feature2_text' => 'Feature2 Text',
            'feature3_red' => 'Feature3 Red',
            'feature3_text' => 'Feature3 Text',
            'visit_count' => 'Visit Count',
        ];
    }

    public function getContents() {
        return $this->hasMany(Content::class, ['article_id' => 'id'])->orderBy(['sort_order' => SORT_ASC]);
    }

    public function getHuman() {
        return $this->hasOne(Human::class, ['id' => 'human_id']);
    }

    public function getCategory() {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public function beforeDelete()
    {
        foreach($this->contents as $content) {
            $content->delete();
        }

        return parent::beforeDelete();
    }

    public function getItems($category_id = 0) {
        $articleQuery = Article::find();

        if ($category_id !== 0) {
            $articleQuery = $articleQuery->where(['category_id' => $category_id]);
        }

        $pages = new \yii\data\Pagination([
            'totalCount' => $articleQuery->count(),
            'defaultPageSize' => 10,
            'pageSizeParam' => 'per_page',
            'forcePageParam' => false,
            'pageSizeLimit' => 200
        ]);

        $articles = $articleQuery->limit($pages->limit)
            ->offset($pages->offset)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return [$articles, $pages];
    }

    public function getUrl() {
        return Url::to(['site/view', 'alias' => $this->category->slug, 'alias2' => $this->slug]);
    }

    public function getNearArticles() {
        $articles = Article::find()
            ->where(['category_id' => $this->category_id])
            ->andWhere(['!=', 'id', $this->id])
            ->limit(3)
            ->orderBy(new Expression('rand()'))
            ->all();

        return $articles;
    }
}
