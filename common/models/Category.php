<?php

namespace common\models;

use himiklab\sortablegrid\SortableGridBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property string|null $near_h1
 * 
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $seo_title
 * @property string|null $seo_h1
 * @property string|null $seo_description
 * @property int|null $sort_order
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }
    
    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'sort_order'
            ],
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => \nsept\behaviors\CyrillicSlugBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique' => true,
            ],
        ];
    }

    public function getUrl() {
        return Url::toRoute(['site/view', 'alias' => $this->slug]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'sort_order'], 'integer'],
            [['seo_description'], 'string'],
            [['name', 'slug', 'seo_title', 'seo_h1', 'near_h1'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'Урл',
            'seo_title' => 'Seo Title',
            'seo_h1' => 'Seo H1',
            'seo_description' => 'Seo Description',
            'near_h1' => 'Текст возле h1',
        ];
    }
}
