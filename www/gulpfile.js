const { src, dest, parallel, watch } = require('gulp');
const gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps');
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    imagemin = require('gulp-imagemin'),
    clone = require('gulp-clone'),
    clonesink = clone.sink(),
    plumber = require('gulp-plumber'),
    webp = require('gulp-webp'),
    cacheFiles = require('gulp-cache-files'),
    babel = require('gulp-babel'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

exports.imagessvg = function(){
    return src('svg/*.svg')
        .pipe(imagemin())
        .pipe(gulp.dest('svg-min'));
};

function styles() {
    return src('scss/app.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({stream: true}));
}

exports.default = function() {
    styles();
    watch('scss/app.scss', styles);
};

exports.styles = styles();