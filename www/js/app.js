$(document).ready(function() {

    // mob-menu
    $('.hamburger').click(function(){
      $('.main-nav').addClass('opened'); 
    });

    $('.close-btn button').click(function(){
      $('.main-nav').removeClass('opened'); 
    });

	//slider w-counter
    $('.carousel').not('.unslick').each(function() {
        var slickInduvidual = $(this);
        var slideCount = null;
        slickInduvidual.on('init', function(event, slick) {
            slideCount = slick.slideCount;
            setSlideCount();
            setCurrentSlideNumber(slick.currentSlide);
        });
        slickInduvidual.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            setCurrentSlideNumber(nextSlide);
        });

        const dots = slickInduvidual.hasClass('reviewsSlider');

        slickInduvidual.slick({
            dots: dots,
            infinite: false,
            fade: true,
            prevArrow: '<button class="slick-prev slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8.85 13.93"><g data-name="Слой 2"><path fill="none" stroke="#454545" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3" d="M7.35 12.43L2.08 6.91 7.35 1.5" data-name="Слой 1"/></g></svg></button>',
            nextArrow: '<button class="slick-next slick-arrow"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8.85 13.93"><g data-name="Слой 2"><path fill="none" stroke="#454545" stroke-linecap="round" stroke-miterlimit="10" stroke-width="3" d="M1.5 1.5l5.26 5.51-5.26 5.42" data-name="Слой 1"/></g></svg></button>',
            slidesToShow: +$(this).attr('data-items-xl'),
            slidesToScroll: +$(this).attr('data-items-xl'),
            responsive: [{
                breakpoint: 1141,
                settings: {
                    slidesToShow: +$(this).attr('data-items-xl'),
                    slidesToScroll: +$(this).attr('data-items-xl'),
                }
            },
            {
                breakpoint: 1140,
                settings: {
                    slidesToShow: +$(this).attr('data-items-lg'),
                    slidesToScroll: +$(this).attr('data-items-lg'),
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: +$(this).attr('data-items-md'),
                    slidesToScroll: +$(this).attr('data-items-md'),
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: +$(this).attr('data-items-sm'),
                    slidesToScroll: +$(this).attr('data-items-sm'),
                }
            }
            ]
        });

        function setSlideCount() {
            var $el = slickInduvidual.closest('.section').find('.to');
            $el.text((slideCount > 9) ? slideCount : '0' + +slideCount);
        }

        function setCurrentSlideNumber(currentSlide) {
            var $el = slickInduvidual.closest('.section').find('.from');
            $el.text((currentSlide > 8) ? currentSlide + 1 : '0' + +(currentSlide + 1));
        }

        $(' button.prev').click(function() {
            $(this).closest('section').find(".slider-default").slick('slickPrev');
        });
        $(' button.next').click(function() {
            $(this).closest('section').find(".slider-default").slick('slickNext');
        });
    });
    $('.js-to-form').on('click', () => {
        $("html, body").stop().animate({scrollTop: $('#form').offset().top}, 301, 'swing');
    });
    $('.sendForm').on('beforeSubmit', (event) => {
        event.preventDefault();
        var form = $(event.currentTarget);

        if (form.hasClass('sending')) {
            return false;
        }
        
        var formData = new FormData(form.get(0));
        var xhr = new XMLHttpRequest();

        form.addClass('sending');

        xhr.open("POST", "/mail/index");
        xhr.send(formData);

        xhr.upload.onprogress = () => {

        };

        xhr.onreadystatechange = () => {
            if (xhr.readyState == 4){
                if (xhr.status == 200){
                    var response = xhr.responseText;

                    if (response == 'success') {
                        const button = form.find('[type="submit"]');

                        if (button.hasClass('js-button')) {
                            button.addClass('success').text('Успешно отправлено!');

                            setTimeout(() => {
                                button.removeClass('success').text(button.data('text'));
                                form.removeClass('sending');
                            }, 3000);
                        } else if (button.hasClass('js-icon-button')) {
                            const image = button.find('img');

                            button.addClass('success');
                            image.attr('src', '/img/check.png');

                            setTimeout(() => {
                                button.removeClass('success').text(button.data('text'));
                                image.attr('src', button.data('icon'));
                                form.removeClass('sending');
                            }, 3000);
                        }

                        yaCounter38200125.reachGoal('ORDER');
                        fbq('track', 'Lead');
                    } else {
                        alert('Ошибка');
                    }
                } else {
                    console.log('error status');
                }
            }
        };
        
        return false;
    });
});
